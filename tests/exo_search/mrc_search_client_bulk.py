#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os, sys
import logging
import time
import json
import copy

import grpc

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from common.config import Config
from maum.brain.qa.mrc_nlqa import NaturalLanguageQuestionAnswering
from maum.brain.qa import semanticSearch_pb2_grpc, semanticSearch_pb2
from maum.brain.qa.utils import util
from maum.brain.mrc.mrc_pb2 import MrcInput, MrcPassage, MRCStub

#==========CONSTANT VARIABLE==========
WINDOW_SIZE_LIST = [5, 7]
DOC_THRESHOLD_LIST = [0]
DOC_CNT = 5
BULK_TYPE = "each"   # each, sum, both
DOC_TYPE = "news"  # news, all
#=====================================

def mrc(q, para_list, server, port):
    channel = grpc.insecure_channel('{}:{}'.format(server, port))
    client = MRCStub(channel)
    mrc_input = MrcInput()
    mrc_input.question = q
    for p in para_list:
        p_input = MrcPassage()
        p_input.original = p["original"]
        p_input.morp = str(p["morp"])
        p_input.words.extend(p["words"])
        mrc_input.passages.extend([p_input])
    mrc_outputs = client.SendQuestion(mrc_input)
    return mrc_outputs.answers

def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

def get_passage_dict(bm25_ps, bulk_type=BULK_TYPE):
    result_dict = dict()
    if bulk_type == "each":
        for p in bm25_ps.semanticResults:
            doc_morp_list = list()
            doc_word_list = list()
            for sent in p.passageMorp:
                morp_list, word_list = util.get_tree_result([eval(sent)["sentence"][1]], True)
                doc_morp_list += morp_list
                doc_word_list += word_list
            if p.docType in result_dict:
                result_dict[p.docType].append({"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list,
                                               "rank": p.rank,
                                               "weight": p.weight})
            else:
                result_dict[p.docType] = [{"original": " ".join(doc_word_list),
                                           "morp": doc_morp_list,
                                           "words": doc_word_list,
                                           "rank": p.rank,
                                           "weight": p.weight}]
    elif bulk_type == "sum":
        doc_morp_list = list()
        doc_word_list = list()
        weight_sum = 0
        for p in bm25_ps.semanticResults:
            for sent in p.passageMorp:
                morp_list, word_list = util.get_tree_result([eval(sent)["sentence"][1]], True)
                doc_morp_list += morp_list
                doc_word_list += word_list
                weight_sum += p.weight
        result_dict["document_sum"] = [{"original": " ".join(doc_word_list),
                                        "morp": doc_morp_list,
                                        "words": doc_word_list,
                                        "rank": 1,
                                        "weight": weight_sum}]
    elif bulk_type == "both":
        result_dict_each = get_passage_dict(bm25_ps, bulk_type="each")['each']
        result_dict_sum = get_passage_dict(bm25_ps, bulk_type="sum")['sum']
        return_dict = {"each": result_dict_each, "sum": result_dict_sum}
        return return_dict
    else:
        logging.error("Your bulk type is wrong!")
    return_dict = {bulk_type: result_dict}
    return return_dict


if __name__ == "__main__":
    conf = Config()
    conf.init("minds-qa.conf")
    nlqa = NaturalLanguageQuestionAnswering("172.31.30.213:" + conf.get("minds-qa.question-analysis.port"))
    se_channel = grpc.insecure_channel('172.31.25.34:30052')
    search = semanticSearch_pb2_grpc.SemanticSearchServiceStub(se_channel)

    input_dir = "../testcase_sample"
    output_dir = "../testcase_result"
    for WINDOW_SIZE in WINDOW_SIZE_LIST:
        for DOC_THRESHOLD in DOC_THRESHOLD_LIST:
            print("Start W{}, D{}".format(WINDOW_SIZE, DOC_THRESHOLD))
            if BULK_TYPE == "both":
                for t in ["each", "sum"]:
                    result_file = os.path.join(output_dir, "testcase_result_mrc_all_w{}_doc{}_{}{}_{}.tsv").\
                        format(WINDOW_SIZE, DOC_THRESHOLD, DOC_TYPE, DOC_CNT, t)
                    exec("f_{} = open(result_file, 'a')".format(t))
            else:
                result_file = os.path.join(output_dir, "testcase_result_mrc_all_w{}_doc{}_{}{}_{}.tsv").\
                    format(WINDOW_SIZE, DOC_THRESHOLD, DOC_TYPE, DOC_CNT, BULK_TYPE)
                exec("f_{} = open(result_file, 'a')".format(BULK_TYPE))

            header = True
            with open(os.path.join(input_dir, "testcase_all_v3.1.txt")) as f:
                for line in f:
                    if header:
                        header = False
                        continue
                    item = line.strip().split("\t")
                    # cate1, cate2, cate3, q_id, question, real_answer,
                    # model_answer, confidence, search_time, mrc_time, passage_cnt_list
                    line_result = item
                    input_q = item[4].strip('"')

                    logging.info("q: {}".format(input_q))

                    # 질문분석
                    nlqa_result = nlqa.analyze(input_q)
                    start_time = time.time()
                    sentences = json.loads(nlqa_result)["orgQInfo"]["orgQUnit"]["ndoc"]["sentence"]
                    q_morp = util.get_tree_result(sentences, original_token=False)
                    exoSearchInput = semanticSearch_pb2.SemanticSearchInput(kbResult=nlqa_result,
                                                                            thresHold=DOC_THRESHOLD,
                                                                            windowSize=WINDOW_SIZE)
                    # 시멘틱 검색
                    start_time = time.time()
                    bm25_ps = search.SearchBM25(exoSearchInput, timeout=10)
                    #tic_ps = search.SearchTIC(exoSearchInput)
                    #boolean_ps = search.SearchBoolean(exoSearchInput)
                    search_time = time.time() - start_time
                    print("--- bm25 search: {} seconds ---".format(search_time))

                    start_time = time.time()
                    result_dicts = get_passage_dict(bm25_ps)
                    for b_type, result_dict in result_dicts.items():
                        para_list = list()
                        print("b_type: {}".format(b_type))
                        passage_cnt_list = [len(result_dict[y]) for y in result_dict]
                        print("Final ctx cnt: {}".format(passage_cnt_list))
                        if len(passage_cnt_list) <= 0:
                            print("There's no passage.")
                            line_result.append("no_passage")
                        else:
                            for k in result_dict.keys():
                                for p in result_dict[k]:
                                    para_list.append(p)
                            mrc_outputs = mrc(str(q_morp), para_list, server='127.0.0.1', port=50001)
                            mrc_time = time.time() - start_time
                            print("--- MRC: {} seconds ---".format(mrc_time))

                            start_time = time.time()
                            answer_dict = dict()
                            q_list = list()
                            for para, mrc_output in zip(para_list, mrc_outputs):
                                line = copy.copy(item)
                                line.extend([str(para['rank']), str(para['weight']), para['original'],
                                             mrc_output.answer.encode('utf-8'), str(mrc_output.prob)])
                                q_list.append("\t".join(line))
                            write_line = "{}\n".format('\n'.join(q_list))
                            exec("f_{}.write(write_line)".format(b_type))
    print("finish all")
