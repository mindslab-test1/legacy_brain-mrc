#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os, sys
import logging
import time
import json

import grpc
import workerpool

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from maum.brain.qa.mrc_nlqa import NaturalLanguageQuestionAnswering
from maum.brain.qa import semanticSearch_pb2_grpc, semanticSearch_pb2
from maum.brain.qa.utils import util
from maum.brain.mrc.mrc_pb2 import MrcInput, MrcPassage, MRCStub


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

logger = logging.getLogger(__name__)
logger.setLevel(logging.CRITICAL)
fh = logging.FileHandler('output.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s',
                              datefmt='%Y-%m-%d_%T %Z')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(ch)

class MrcClient():
    search_channel = None
    search_client = None

    def __init__(self):
        # ==========CONSTANT VARIABLE==========
        self.WINDOW_SIZE = 7
        self.DOC_THRESHOLD = 0
        self.DOC_TYPE = "news"
        self.ANSWER_THRESHOLD = 0.7
        # =====================================
        self.nlqa = NaturalLanguageQuestionAnswering("172.31.30.213:9861")
        self.search_channel = grpc.insecure_channel('172.31.25.34:30052')
        self.search = semanticSearch_pb2_grpc.SemanticSearchServiceStub(self.search_channel)
        self.mrc_channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
        self.mrc_client = MRCStub(self.mrc_channel)
        # for time check
        self.entire_start_time = None
        self.start_time = None
        self.mrc_time_list = []

    def time_check(self, stage):
        time_check = float(time.time() - self.start_time)
        logger.info("{}: {:.5}s".format(stage, time_check))
        self.start_time = time.time()
        if stage == "MRC":
            self.mrc_time_list.append(time_check)

    def run(self, question):
        global q
        global para_list

        logger.info("Input: {}".format(question))
        self.start_time = self.entire_start_time = time.time()

        # question analysis
        nlqa_result = self.nlqa.analyze(question)  # TODO: replace it with exobrain result
        self.time_check(stage="nlqa")

        sentences = json.loads(nlqa_result)["orgQInfo"]["orgQUnit"]["ndoc"]["sentence"]
        q_morp = util.get_tree_result(sentences, original_token=False)
        exoSearchInput = semanticSearch_pb2.SemanticSearchInput(kbResult=nlqa_result,
                                                                thresHold=self.DOC_THRESHOLD,
                                                                windowSize=self.WINDOW_SIZE)
        self.time_check(stage="Prepare bm25 search")

        # semantic search
        bm25_ps = self.search.SearchBM25(exoSearchInput, timeout=10)
        #tic_ps = search.SearchTIC(exoSearchInput)
        #boolean_ps = search.SearchBoolean(exoSearchInput)
        self.time_check(stage="Bm25 search")

        result_dict = dict()
        for p in bm25_ps.semanticResults:
            if self.DOC_TYPE == "news" and p.docType != "news_passage":
                continue
            doc_morp_list = list()
            doc_word_list = list()
            for sent in p.passageMorp:
                morp_list, word_list = util.get_tree_result([eval(sent)["sentence"][1]], True)
                doc_morp_list += morp_list
                doc_word_list += word_list
            if p.docType in result_dict:
                result_dict[p.docType].append({"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list})
            else:
                result_dict[p.docType] = [{"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list}]

        passage_cnt_list = [len(result_dict[y]) for y in result_dict]
        logger.debug("Retrieved document cnt/type: {}".
                          format(zip(result_dict.keys(), passage_cnt_list)))
        if len(passage_cnt_list) <= 0:
            logger.error("There's no passage. Return None")
            return "", False
        else:
            para_list = list()
            for k in result_dict.keys():
                for p in result_dict[k]:
                    para_list.append(p)

            workers = 20
            n = range(workers)

            q = str(q_morp)
            para_list = para_list

            mrc_input = MrcInput()
            mrc_input.question = q
            for p in para_list:
                p_input = MrcPassage()
                p_input.original = p["original"]
                p_input.morp = str(p["morp"])
                p_input.words.extend(p["words"])
                mrc_input.passages.extend([p_input])

            self.time_check(stage="Prepare MRC")

            channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
            global stub
            stub = MRCStub(channel)
            pool = workerpool.WorkerPool(size=workers)
            pool.map(mrc, [mrc_input]*workers)
            pool.shutdown()
            pool.wait()
            self.time_check(stage="MRC")

def init_stub():
  global mrc_client
  channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
  mrc_client = MRCStub(channel)

def mrc(mrc_input):
    mrc_outputs = stub.SendQuestion(mrc_input)
    print(mrc_outputs.answers[0].answer)

if __name__ == "__main__":
    mc = MrcClient()
    for q in ["걸그룹 모모랜드에서 아파서 빠지는 멤버는", "직캠이 뭐야", "운빨로맨스는 어느 채널이야"]:
        a = mc.run(q)
        print(mc.mrc_time_list)
        logger.critical("Average MRC: {}".format(sum(mc.mrc_time_list)/len(mc.mrc_time_list)))
