#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os, sys
import logging
import time
import json

import grpc
import workerpool

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from maum.brain.qa.mrc_nlqa import NaturalLanguageQuestionAnswering
from maum.brain.qa import semanticSearch_pb2_grpc, semanticSearch_pb2
from maum.brain.qa.utils import util
from maum.brain.mrc.mrc_pb2 import MrcInput, MrcPassage, MRCStub


def merge_dicts(*dict_args):
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

logger = logging.getLogger(__name__)
logger.setLevel(logging.CRITICAL)
fh = logging.FileHandler('output.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='[%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s',
                              datefmt='%Y-%m-%d_%T %Z')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logger.addHandler(ch)

class MrcClient():
    search_channel = None
    search_client = None

    def __init__(self):
        # ==========CONSTANT VARIABLE==========
        self.WINDOW_SIZE = 7
        self.DOC_THRESHOLD = 0
        self.DOC_TYPE = "news"
        self.ANSWER_THRESHOLD = 0.7
        # =====================================
        self.nlqa = NaturalLanguageQuestionAnswering("172.31.30.213:9861")
        self.search_channel = grpc.insecure_channel('172.31.25.34:30052')
        self.search = semanticSearch_pb2_grpc.SemanticSearchServiceStub(self.search_channel)
        self.mrc_channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
        self.mrc_client = MRCStub(self.mrc_channel)
        # for time check
        self.entire_start_time = None
        self.start_time = None
        self.nlqa_time_list = []
        self.pre_bm25_time_list = []
        self.bm25_time_list = []
        self.pre_mrc_time_list = []
        self.mrc_time_list = []
        self.entire_time_list = []

    def time_check(self, stage, start_time):
        time_check = float(time.time() - start_time)
        from_start = float(time.time() - self.entire_start_time)
        logger.info("{}: {:.5}s".format(stage, time_check))
        if stage == "nlqa":
            self.nlqa_time_list.append(time_check)
        elif stage == "Prepare bm25 search":
            self.pre_bm25_time_list.append(time_check)
        elif stage == "Bm25 search":
            self.bm25_time_list.append(time_check)
        elif stage == "Prepare MRC":
            self.pre_mrc_time_list.append(time_check)
        elif stage == "MRC":
            self.mrc_time_list.append(time_check)
            self.entire_time_list.append(from_start)
        return time.time()

    def run(self, question):
        global q
        global para_list

        logger.info("Input: {}".format(question))
        start_time = time.time()

        # question analysis
        nlqa_result = self.nlqa.analyze(question)  # TODO: replace it with exobrain result
        self.entire_start_time = time.time()
        start_time = self.time_check(stage="nlqa",start_time=start_time)

        sentences = json.loads(nlqa_result)["orgQInfo"]["orgQUnit"]["ndoc"]["sentence"]
        q_morp = util.get_tree_result(sentences, original_token=False)
        exoSearchInput = semanticSearch_pb2.SemanticSearchInput(kbResult=nlqa_result,
                                                                thresHold=self.DOC_THRESHOLD,
                                                                windowSize=self.WINDOW_SIZE)
        start_time = self.time_check(stage="Prepare bm25 search",start_time=start_time)

        # semantic search
        bm25_ps = self.search.SearchBM25(exoSearchInput, timeout=10)
        #tic_ps = search.SearchTIC(exoSearchInput)
        #boolean_ps = search.SearchBoolean(exoSearchInput)
        start_time = self.time_check(stage="Bm25 search",start_time=start_time)

        result_dict = dict()
        for p in bm25_ps.semanticResults:
            if self.DOC_TYPE == "news" and p.docType != "news_passage":
                continue
            doc_morp_list = list()
            doc_word_list = list()
            for sent in p.passageMorp:
                morp_list, word_list = util.get_tree_result([eval(sent)["sentence"][1]], True)
                doc_morp_list += morp_list
                doc_word_list += word_list
            if p.docType in result_dict:
                result_dict[p.docType].append({"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list})
            else:
                result_dict[p.docType] = [{"original": " ".join(doc_word_list),
                                               "morp": doc_morp_list,
                                               "words": doc_word_list}]

        passage_cnt_list = [len(result_dict[y]) for y in result_dict]
        logger.debug("Retrieved document cnt/type: {}".
                          format(zip(result_dict.keys(), passage_cnt_list)))
        if len(passage_cnt_list) <= 0:
            logger.error("There's no passage. Return None")
            return "", False
        else:
            para_list = list()
            for k in result_dict.keys():
                for p in result_dict[k]:
                    para_list.append(p)
            start_time = self.time_check(stage="Prepare MRC",start_time=start_time)
            q = str(q_morp)
            para_list = para_list

            mrc_input = MrcInput()
            mrc_input.question = q
            for p in para_list:
                p_input = MrcPassage()
                p_input.original = p["original"]
                p_input.morp = str(p["morp"])
                p_input.words.extend(p["words"])
                mrc_input.passages.extend([p_input])

            mrc(mrc_input)
            start_time = self.time_check(stage="MRC",start_time=start_time)

def init_stub():
  global mrc_client
  channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
  mrc_client = MRCStub(channel)

def mrc(mrc_input):
    mrc_outputs = stub.SendQuestion(mrc_input)
    print(mrc_outputs.answers[0].answer)

if __name__ == "__main__":
    output_file = "../testcase_result/bulk_speed_test.tsv"
    workers_list = [50]
    list_no_list = [1]
    mc = MrcClient()
    q_list1 = ["걸그룹 모모랜드에서 아파서 빠지는 멤버는", "직캠이 뭐야", "운빨로맨스는 어느 채널이야",
              "월화드라마 시청률 1위 알려줘", "미국에서 파이어볼 사고 난 게 어디야"]
    q_list2 = ["로엔엔터테인먼트 어디랑 드라마 제작사 공동설립했어", "서울의 인구당 주택수를 알려줘",
               "여자배구 한일전 시청률 제일 안나온곳이 어디야", "잔느 모로가 여우주연상을 탄 작품이 뭐야",
               "영화 박열이 개봉하는 날짜는"]
    for list_no in list_no_list:
        for workers in workers_list:
            for q in eval("q_list{}".format(list_no)):
                channel = grpc.insecure_channel('{}:{}'.format('172.31.5.106', 50001))
                global stub
                stub = MRCStub(channel)
                pool = workerpool.WorkerPool(size=workers)
                pool.map(mc.run, [q] * workers)
                pool.shutdown()
                pool.wait()
                time.sleep(1)
            logger.info(mc.nlqa_time_list)
            nlqa_average = sum(mc.nlqa_time_list)/len(mc.nlqa_time_list)
            pre_bm25_average = sum(mc.pre_bm25_time_list)/len(mc.pre_bm25_time_list)
            bm25_average = sum(mc.bm25_time_list)/len(mc.bm25_time_list)
            pre_mrc_average = sum(mc.pre_mrc_time_list)/len(mc.pre_mrc_time_list)
            mrc_average = sum(mc.mrc_time_list)/len(mc.mrc_time_list)
            entire_average = sum(mc.entire_time_list)/len(mc.entire_time_list)
            logger.critical("Average MRC: {}".format(mrc_average))
            write_list = [str(list_no), str(workers),
                          str(nlqa_average), str(pre_bm25_average), str(bm25_average),
                          str(pre_mrc_average), str(mrc_average), str(entire_average)]
            with open(output_file, "a") as f:
                f.write("\t".join(write_list))
                f.write("\n")
            mc.nlqa_time_list = []
            mc.bm25_time_list = []
            mc.mrc_time_list = []
            mc.entire_time_list = []
            time.sleep(2)
