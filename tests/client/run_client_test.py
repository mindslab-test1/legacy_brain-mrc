#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os, sys
import json

import grpc
from google.protobuf import json_format

sys.path.append(os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2 as mrcrnr
import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2_grpc as mrcrnr_grpc
import google.protobuf.empty_pb2 as empty

def main(server, port, type, uuid=None):
    channel = grpc.insecure_channel('{}:{}'.format(server, port))
    client = mrcrnr_grpc.MrcResolverStub(channel)

    if type == "set_model":  # start train
        # create a message from JSON string
        with open('run_client_test.json') as json_data:
            d = json_data.read()
        parsed_pb_model = json_format.Parse(d, mrcrnr.MrcModel(), ignore_unknown_fields=False)
        print("Start {}".format(type))
        return_value = client.SetModel(parsed_pb_model)
    elif type == "get_models":
        d = json.dumps('{}')
        d = json.loads(d)
        parsed_pb_empty = json_format.Parse(d, empty.Empty(), ignore_unknown_fields=False)
        return_value = client.GetModels(parsed_pb_empty)
    elif type == "get_model":
        print(uuid)
        d = json.dumps('{"uuid": "' + uuid + '"}')
        d = json.loads(d)
        parsed_pb_key = json_format.Parse(d, mrcrnr.MrcModelKey(), ignore_unknown_fields=False)
        return_value = client.GetModel(parsed_pb_key)
    elif type == "stop":
        print(uuid)
        d = json.dumps('{"uuid": "' + uuid + '"}')
        d = json.loads(d)
        parsed_pb_key = json_format.Parse(d, mrcrnr.MrcModelKey(), ignore_unknown_fields=False)
        return_value = client.Stop(parsed_pb_key)
    print(return_value)

if __name__ == '__main__':
    type = sys.argv[1]
    if type in ('get_model', 'stop'):
        uuid = sys.argv[2]
    else:
        uuid = None
    main(server='127.0.0.1', port=54000, type=type, uuid=uuid)
