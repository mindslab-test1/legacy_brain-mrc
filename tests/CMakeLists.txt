cmake_minimum_required(VERSION 2.8.12)

install(PROGRAMS client/run_client_test.json
    DESTINATION samples/mrc)

install(PROGRAMS client/run_client_test.py
    DESTINATION samples/mrc
    RENAME run_client_test
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )

install(PROGRAMS client/run_client_test_qa.py
    DESTINATION samples/mrc
    RENAME run_client_test_qa
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )
