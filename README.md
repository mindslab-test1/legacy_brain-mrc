# brain-mrc

## 버전
- v1.1.0
- release date: 2018.01.04

## 주의사항
- 머신을 재부팅 시 nvidia 드라이버가 잡히지 않는 버그가 있습니다
- 재부팅 후에는 `nvidia-smi` 명령어로 반드시 드라이버가 잡히는지 확인하고 만약 `NVIDIA-SMI has failed because it couldn't communicate with the NVIDIA driver. Make sure that the latest NVIDIA driver is installed and running.` 이라는 문구가 뜰 경우 드라이버 재설치를 해 주어야 합니다. 

## 설치
```
git clone https://github.com/mindslab-ai/brain-mrc.git
git fetch
git pull origin canary
./build.sh
```

## 실행 데몬 구동
```
cd ${MAUM_ROOT}/bin   # ${MAUM_ROOT}: MAUM_ROOT로 지정한 위치
workon venv_mrc   # python virtualenv로 진입
./brain-mrcd
```

## 실행 테스트
- 학습된 모델이 `${MAUM_ROOT}/trained/mrc/${uuid}` 안에 존재해야 합니다
- 모델 파일의 경로는 `brain-mrc.conf`에서의 설정에 따라 달라질 수 있습니다
```
cd ${MAUM_ROOT}/samples
workon venv_mrc
./run_client_test set_model    # mrc 모델 로드(리턴값 안에 port 포함되어 있음)
./run_client_test get_models    # 구동한 모델들의 정보 보기
./run_client_test stop '${uuid}'    # 특정 구동 uuid를 input으로 하여 프로세스 종료
./run_client_test_qa -p ${port} -q "데이브 존스 직업이 뭐야"
```

## 환경
- 학습과 서버 구동은 `python>=3.5.1` 환경 필요
- 클라이언트 쪽은 python 버전 무관(2, 3 모두 가능)
- 자세한 환경 관련 가이드라인은 [wiki 링크](https://github.com/mindslab-ai/brain-mrc/wiki) 참고 바랍니다

## 구동 시 필요한 리소스
- AWS S3에서 빌드 시 자동 다운로드되도록 되어 있습니다

## 디렉토리 구조
- `log`: `tests/server`에 들어 있는 데몬 서버를 실행할 경우 로그 파일이 생성되는 곳. 데몬을 실행할 때의 시간으로 파일명 자동 생성됨
- `proto/maum/brain/mrc/train`: train proto가 포함
- `pysrc/maum/brain/mrc`: python 코드가 포함된 디렉토리
  - `aws-s3`: s3에서 자동으로 리소스를 다운로드하도록 하는 스크립트가 포함된 디렉토리
  - `core`: 학습 관련 core 소스가 포함된 디렉토리
  - `server`: 데몬 서버를 실행시키는 스크립트가 포함된 디렉토리
  - `tlo`: TLO 로깅 관련 스크립트가 포함된 디렉토리
  - `train`: 학습 관련 스크립트가 포함된 디렉토리
  - `utils`: 기타 utility 스크립트가 포함된 디렉토리
- `tests`: 데몬 실행과 클라이언트 쪽 코드가 포함된 디렉토리
  - `client`
  - `exo_search`: 엑소브레인 시멘틱 검색까지 연동하여 테스트할 수 있는 코드
- `CHANGELOG.md`: 릴리즈 단위의 change log
- `requirements.txt`: 필요한 python 라이브러리 목록
- `build.sh`: 설치를 위한 build 파일
- `README.md`

## 컴포넌트 구분
- common: 컴포넌트를 특정할 수 없는 경우
- build: 빌드와 관련된 부분
- pre-pro(common): 학습을 위한 정제 과정 중 언어 무관하게 공통인 부분
- pre-pro(ko): 학습을 위한 정제 과정 중 한국어에 초점이 맞춰진 부분
- pre-pro(en): 학습을 위한 정제 과정 중 영어에 초점이 맞춰진 부분
- run(common): 실행 부분 중 공통인 기능
- run(prediction): 실행 부분 중 모델이 인덱스 형태로 예측값을 도출하는 부분까지를 지칭
- run(answer): 인덱스 형태의 예측값을 사람이 이해할 수 있는 최종 정답으로 변환하는 부분까지를 지칭
- train: 학습 관련된 모든 단계
- client: 클라이언트 측과 관련된 기능
- application: MRC를 이용하여 다른 모듈과 융합했거나 응용한 경우의 기능
