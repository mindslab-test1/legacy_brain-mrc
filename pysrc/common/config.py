# -*- coding:utf-8 -*-

"""
설정 파일의 내용을 읽어서 이를 반환한다.
싱글톤으로 동작한다.
"""

import sys
import os

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(
                    *args, **kwargs)
        return cls._instances[cls]


class Config(object):
    """
    설정파일을 로딩한다.
    """
    __metaclass__ = Singleton
    config_file = ''
    config_map = {}
    maum_root = ''
    conf_path = ''

    def __init__(self):
        self.maum_root = self.get_maum_root()

        curpath = os.path.realpath(sys.argv[0])
        if sys.version_info < (3, 0):
            if os.environ.has_key('PYTHONPATH') == True:
                # for simple server connect test and unit test
                curpath = os.environ.get('PYTHONPATH').split(":")[0]
                self.conf_path = os.path.join(curpath.split('pysrc')[0], 'config')
            else:
                # for installed environment
                self.conf_path = os.path.realpath(
                        os.path.join(self.maum_root, 'etc'))
            pass
        else:
            if 'PYTHONPATH' in os.environ:
                # for simple server connect test and unit test
                curpath = os.environ.get('PYTHONPATH').split(":")[0]
                self.conf_path = os.path.join(curpath.split('pysrc')[0], 'config')
            else:
                # for installed environment
                self.conf_path = os.path.realpath(
                        os.path.join(self.maum_root, 'etc'))
            pass

    def init(self, filename):
        self.find_config_file(filename)
        self.parse_config()

    def find_config_file(self, filename):
        conf_file = os.path.realpath(os.path.join(self.conf_path, filename))
        if not os.path.exists(conf_file):
            raise RuntimeError('config file not exist:' + conf_file)
        self.config_file = conf_file

    def get_maum_root(self):
        maum_root = os.environ.get('MAUM_ROOT')
        if maum_root is None:
            home = os.environ.get('HOME')
            if home is not None:
                maum_root = os.path.join(home, 'maum')
            else:
                exe_path = os.path.realpath(sys.argv[0])
                bin_path = os.path.dirname(exe_path)
                p_path = os.path.realpath(bin_path + '/..')
                print('set as maum_root = ', p_path)
                maum_root = p_path
        return maum_root

    def parse_config(self):
        with open(self.config_file) as myfile:
            while True:
                line = myfile.readline()
                if not line:
                    break
                line = line.strip()
                if len(line) == 0 or line[0] == '#':
                    continue
                name, var = line.partition("=")[::2]
                var = var.strip().replace('${MAUM_ROOT}', self.maum_root)
                self.config_map[name.strip()] = var

    def get(self, field):
        if sys.version_info < (3, 0):
            if self.config_map.has_key(field):
                return self.config_map[field]
            else:
                return ''
        else:
            if field in self.config_map:
                return self.config_map[field]
            else:
                return ''