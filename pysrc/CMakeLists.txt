cmake_minimum_required(VERSION 2.8.12)

install(DIRECTORY ./maum/brain/mrc
    DESTINATION lib/python/maum/brain
    FILES_MATCHING PATTERN "*.py")

install(PROGRAMS maum/brain/mrc/run/mrcd.py
    DESTINATION bin
    RENAME brain-mrcd
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )

install(PROGRAMS maum/brain/mrc/run/run_server/mrc_model_loader.py
    DESTINATION bin
    RENAME brain-mrc-proc
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
    )