#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys
import grpc, json

from google.protobuf import empty_pb2
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../../minds/lib/python")
sys.path.append(lib_path)

from exo_search.minds.qa import qa_pb2
from exo_search.minds.qa import cd_pb2
from common.config_qa import Config

class DeduceCandidate:
    conf = Config()
    stub = None

    def __init__(self):
        remote = "125.132.250.243:"+self.conf.get("minds-qa.deduce-candidate.port")
        #remote = "13.124.164.53:"+self.conf.get("minds-qa.deduce-candidate.port")
        channel = grpc.insecure_channel(remote)
        self.stub = qa_pb2.QuestionAnswerServiceStub(channel)

    def deduce_candidate(self, text, start_index=1, end_index=5):
        in_text = cd_pb2.DeductionCandidateInputText()
        in_text.text = text
        in_text.start_index = start_index
        in_text.end_index = end_index
        result = self.stub.DeduceCandidate(in_text)
        result = result.result
        token = result.split("||0||")
        json_result = json.loads(token[1])
        return json_result


if __name__ == "__main__":
    conf = Config()
    conf.init("minds-qa.conf")

    deduce_candidate = DeduceCandidate()
    with open("cd_input", "r") as f:
        line_list = f.readlines()

    cd_input = ""
    for line in line_list:
        cd_input = cd_input + line + "\n"
    cd_input = cd_input.strip()
    temp_start_index = 1
    temp_end_index = 5
    result = deduce_candidate.deduce_candidate(cd_input, temp_start_index, temp_end_index)
    print(result)

