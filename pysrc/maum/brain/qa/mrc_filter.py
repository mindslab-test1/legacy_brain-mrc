#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import os
import sys

import grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path+"/../lib/python")
sys.path.append(lib_path)

from exo_search.minds.qa import qa_pb2
from exo_search.minds.qa import filter_pb2
from common.config import Config

class FilterAnswerCandidate:
    conf = Config()
    stub = None

    def __init__(self):
        remote = "localhost:"+self.conf.get("minds-qa.filter-answer-candidate.port")
        channel = grpc.insecure_channel(remote)
        self.stub = qa_pb2.QuestionAnswerServiceStub(channel)

    def filter_answer_candidate(self, text):
        in_text = filter_pb2.FilterAnswerCandidateInputText()
        in_text.text = text
        result = self.stub.FilterAnswerCandidate(in_text)
        result = result.result
        token = result.split("||0||")
        json_result = json.loads(token[1])
        return json_result


if __name__ == "__main__":
    conf = Config()
    conf.init("minds-qa.conf")
    filter_answer = FilterAnswerCandidate()
    
    with open("filter_answer_input", "r") as f:
        line_list = f.readlines()

    input_text = ""
    
    for line in line_list:
        input_text = input_text + "\n" + line

    input_text = input_text.strip()
    result = filter_answer.filter_answer_candidate(input_text)
    print(result)
