#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import os
import sys

import grpc
from mrc_passage_search import PassageSearch

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + "/../lib/python")
sys.path.append(lib_path)

from exo_search.minds.qa import qa_pb2
from exo_search.minds.qa import cangen_pb2
from common.config import Config

# ========== CONSTANT VARIABLE ==========
WINDOW_SIZE = 6
# =======================================


class GenerateUnstructuredCandidate:
    conf = Config()
    stub = None

    def __init__(self, remote, remote_ps):
        self.passage_search = PassageSearch(remote_ps)
        #remote = "125.132.250.243:" + self.conf.get("minds-qa.generate-unstructured-candidate.port")
        #remote = "172.31.30.213:" + self.conf.get("minds-qa.generate-unstructured-candidate.port")
        channel = grpc.insecure_channel(remote)
        self.stub = qa_pb2.QuestionAnswerServiceStub(channel)

    def search_unstructured_candidate(self, text):
        in_text = cangen_pb2.UnstructuredCandidateInputText()
        in_text.text = text 
        result = self.stub.GenerateUnstructuredCandidate(in_text)
        result_unstructured_candidate = result.result
        result_unstructured_candidate = result_unstructured_candidate.encode("utf-8")
        token = result_unstructured_candidate.split("||0||")
        json_result = json.loads(token[1])
        result_dict = self.passage_search.get_passage_search_result_list(json_result, WINDOW_SIZE)
        return result_dict


if __name__ == "__main__":
    conf = Config()
    conf.init("minds-qa.conf")
    passage = GenerateUnstructuredCandidate()

    with open("passage_input", "r") as f:
        line_list = f.readlines()

    input_text = ""
    for line in line_list:
        input_text = input_text + "\n" + line

    input_text = input_text.strip()
    result = passage.search_unstructured_candidate(input_text)
    print(result)
