#! /usr/bin/python3.5
# -*- coding: UTF-8 -*-

import grpc

from maum.brain.nlp import nlp_pb2
from maum.brain.nlp import nlp_pb2_grpc
from maum.common import lang_pb2

remote = "localhost:9823"
channel = grpc.insecure_channel(remote)
stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(channel)

class NLPAnalyzer:
    stub = None

    def __init__(self):
        #self.remote = "localhost:9823"
        self.morp_result = list()
        self.ner_result = list()
        self.sentence_result = list()
        self.morp_result_list = list()

    def __analyze__(self, text):
        # type: (object) -> object
        in_text = nlp_pb2.InputText()
        in_text.text = text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False

        message_result = stub.Analyze(in_text)

        morp_result = list()
        ner_result = list()
        sentence_result = list()
        sentence_morp_dict = ()
        for i in range(len(message_result.sentences)):
            sentence = message_result.sentences[i].text
            sentence_result.append(sentence.strip())
            morp_analysis = message_result.sentences[i].morps
            morp = ""
            for j in range(len(morp_analysis)):
                morp = morp_analysis[j].lemma + "/" + morp_analysis[j].type
            morp_result.append(morp.strip())
            self.sentence_morp_dict[sentence] = morp_result
            self.morp_result_list.append(morp_result)

            ner_analysis = message_result.sentences[i].nes

            ner = ""
            for j in range(len(ner_analysis)):
                ner = ner_analysis[j].text + "/" + ner_analysis[j].type
                if not ner:
                    continue
                else:
                    ner = ner.encode('utf-8').strip()
                    ner_result.append(ner.strip())

        self.morp_result = morp_result
        self.ner_result = ner_result
        self.sentence_result = sentence_result

    def get_tree_result(self, content, original_token=False):
        ret = self.get_all_result(content)
        final_list = list()
        word_list = list()
        for sent in ret.sentences:
            if original_token:
                for word in sent.words:
                    word_list.append(word.text)
            sent_list = list()
            for morph in sent.morph_evals:
                tokens = morph.result.replace("+", "\t").replace("\t/SW", "+/SW").split("\t")
                item_list = list()
                for token in tokens:
                    item = token.split("/")
                    if len(item) > 2:
                        item = ["/"] + [item[-1]]
                    item_list.append("/".join([item[0], item[1].lower()]))
                sent_list.append(item_list)
            final_list.append(sent_list)
        if original_token:
            return final_list, word_list
        else:
            return final_list

    def get_all_result(self, text):
        in_text = nlp_pb2.InputText()
        in_text.text = text
        in_text.lang = lang_pb2.kor
        in_text.split_sentence = True
        in_text.use_tokenizer = False
        in_text.level = 0
        in_text.keyword_frequency_level = 0
        ret = stub.Analyze(in_text)
        return ret

    def map_original_idx(self, morph, original):
        flatten_morph = list()
        for s in morph:
            flatten_morph += s
        map_dic = dict()
        m_idx = 0
        o_idx = 0
        for m, o in zip(flatten_morph, original):
            print(m, o)
            if len(m) == 1:
                map_dic[m_idx] = (o_idx, o_idx+len(o))
                m_idx += 1
                o_idx += len(o) + 1
            else:
                pass_list = list()      # splitted morph ex. 나서+었
                start_from = 0
                fail = False  # beforehand
                for each_m in m:
                    splitted_m = each_m.rsplit("/", 1)[0]
                    location = o.find(splitted_m, start_from)
                    if location >= 0:
                        if fail:
                            o_idx += location
                            fail = False
                        map_dic[m_idx] = (o_idx, o_idx+len(splitted_m))
                        o_idx += len(splitted_m)
                        start_from = location
                    else:
                        pass_list.append((m_idx, m, start_from+o_idx, o))
                        fail = True
                    m_idx += 1
                pass_list_tmp = pass_list
                if len(pass_list) > 0:  # if there is a splitted morphs
                    for pass_item in pass_list:
                        # m_idx, m, start_position
                        backward_until = 1
                        while m_idx > (pass_item[0]+backward_until):
                            try:
                                #만약 그 다음 것도 찾지 못하면 하나 더 뒤로 감
                                map_dic[pass_item[0]] = (pass_item[2], map_dic[pass_item[0]+backward_until][0])
                                pass_list_tmp.remove(pass_item)
                                break
                            except KeyError:
                                print("Error")
                                backward_until += 1
                if len(pass_list_tmp) > 0:
                    for pass_item in pass_list_tmp:
                        map_dic[pass_item[0]] = (pass_item[2], o_idx+len(o))
                o_idx += 1  # space
        map_dic[m_idx] = (o_idx-1, o_idx-1)
        return map_dic

if __name__ == "__main__":
    nlp_analyze = NLPAnalyzer()
    content = """이때 청취자의 성대모사를 맞추기 앞서, 박명수는 “너무 어려운 것 같다."""
    morph_content, word_list = nlp_analyze.get_tree_result(content, original_token=True)
    map_dic = nlp_analyze.map_original_idx(morph_content, word_list)
    print(map_dic)
    print(content[map_dic[20][0]:map_dic[20][1]])
