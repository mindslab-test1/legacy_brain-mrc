#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import os, sys
import logging
import re
import string
import collections
import gc
import pdb

import torch
import numpy
import msgpack
import pandas as pd
import psutil

logger = logging.getLogger('trainer')

def load_data(opt):
    # train set
    with open(opt['train_data'], 'rb') as f:
        data = msgpack.load(f, encoding='utf8')
    train_x, train_q, train_xc, train_qc, train_y, train_x_str, train_q_str, train_qids, vocab_x_list, opt['char2id'] \
        = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], data['a_lst'], data['x_str'], data['q_str'], \
          data['qids'], data['voca_list'], data['char2id']

    # dev set
    with open(opt['dev_data'], 'rb') as f:
        data = msgpack.load(f, encoding='utf8')
    dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids, _, _ \
        = data['x_lst'], data['q_lst'], data['x_c_lst'], data['q_c_lst'], data['a_lst'], data['x_str'], data['q_str'], \
          data['qids'], data['voca_list'], data['char2id']

    logger.info('Train data size: {}, {}, {}, {}, {}, {}'.format(len(train_x), len(train_q), len(train_y), len(train_x_str), len(train_q_str), len(train_qids)))
    logger.info('Dev data size: {}, {}, {}, {}, {}, {}'.format(len(dev_x), len(dev_q), len(dev_y), len(dev_x_str), len(dev_q_str), len(dev_qids)))
    if opt['use_char']:
        opt['char_vocab_size'] = len(opt['char2id'])
        logger.info('train char: {}, {}'.format(len(train_xc), len(train_qc)))
        logger.info('dev char: {}, {}'.format(len(dev_xc), len(dev_qc)))
        logger.info('char_vocab_size: {}'.format(opt['char_vocab_size']))
    # train_y -> train_ys and train ye
    train_ys, train_ye = [], []
    for y in train_y:
        train_ys.append(int(y[1]))
        train_ye.append(int(y[2]))
    # dev_y -> dev_ys and dev ye
    dev_ys, dev_ye = [], []
    for y in dev_y:
        dev_ys.append(int(y[1]))
        dev_ye.append(int(y[2]))

    if opt['use_char']:
        logger.debug('dev_qc[0]: {}'.format(dev_qc[0]))
    logger.debug('dev_x[0]: {}'.format(dev_x[0]))
    logger.debug('dev_q[0]: {}'.format(dev_q[0]))
    logger.debug('dev_y[0]: {}'.format(dev_y[0]))
    logger.debug('dev_ys[0]: {}'.format(dev_ys[0]))
    logger.debug('dev_ye[0]: {}'.format(dev_ye[0]))
    logger.debug('dev_x_str[0]: {}'.format(dev_x_str[0]))
    logger.debug('dev_q_str[0]: {}'.format(dev_q_str[0]))
    logger.debug('dev_qids[0]: {}'.format(dev_qids[0]))

    # load word vocab.
    opt['word2id'] = {}
    for i, w in enumerate(vocab_x_list):
        if w in opt['word2id']:
            try: logger.warning('Warning(vocab_word): {} is exist! - {} {}'.format(w, opt['word2id'][w], i))
            except: pass
        opt['word2id'][w] = i
    if 'eos' not in opt['word2id']:
        i += 1
        vocab_x_list.append('eos')
        opt['word2id']['eos'] = i
        logger.info('"eos" is appended to vocab.')
    logger.info('vocab_x size: {}'.format(i+1))
    opt['pretrained_words'] = True
    opt['vocab_size'] = i+1
    # init char vocab.
    opt['char_padding_idx'] = 0
    # load word embedding
    emb_numpy = load_embedding(opt['emb_file'], opt['word2id'], opt['vocab_size'], opt['embedding_dim'])
    embedding = torch.Tensor(emb_numpy)
    # padding_idx
    # eos for batch padding
    if '<PAD>' in opt['word2id']: padding_idx = opt['word2id']['<PAD>']
    elif '<pad>' in opt['word2id']: padding_idx = opt['word2id']['<pad>']
    elif '</s>' in opt['word2id']: padding_idx = opt['word2id']['</s>']
    elif 'EOS' in opt['word2id']: padding_idx = opt['word2id']['EOS']
    elif 'eos' in opt['word2id']: padding_idx = opt['word2id']['eos']
    elif 'UNK' in opt['word2id']: padding_idx = opt['word2id']['UNK']
    elif 'unk' in opt['word2id']: padding_idx = opt['word2id']['unk']
    else: padding_idx = 0
    opt['padding_idx'] = padding_idx
    logger.info('Batch padding idx: {} {}'.format(padding_idx, vocab_x_list[padding_idx]))

    # exact_match , TF feature
    def make_feature(train_x, train_q, train_x_str, train_q_str):
        for x, q, xs, qs in zip(train_x, train_q, train_x_str, train_q_str): # using word idx
            if len(x) != len(xs) or len(q) != len(qs):
                logger.info(len(x), len(xs), len(q), len(qs))
                pdb.set_trace()
        train_f = []
        #for x, q in zip(train_x, train_q): # using word idx
        for x, q in zip(train_x_str, train_q_str): # using word string
            f = [wid in q for wid in x]
            counter_ = collections.Counter(wid for wid in x)
            total = sum(counter_.values()) + 1e-5
            tf = [counter_[wid] / total for wid in x]
            train_f.append(list(zip(f, tf)))
        return train_f
    train_xf = make_feature(train_x, train_q, train_x_str, train_q_str)
    logger.info('Exact_match and TF features are generated.({})'.format("train_xf"))
    dev_xf = make_feature(dev_x, dev_q, dev_x_str, dev_q_str)
    logger.info('Exact_match and TF features are generated.({})'.format("dev_xf"))
    train_qf = make_feature(train_q, train_x, train_q_str, train_x_str)
    logger.info('Exact_match and TF features are generated.({})'.format("train_qf"))
    dev_qf = make_feature(dev_q, dev_x, dev_q_str, dev_x_str)
    logger.info('Exact_match and TF features are generated.({})'.format("dev_qf"))

    # train, dev data
    if opt['use_char']:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_xc, dev_qc, dev_ys, dev_ye, dev_x_str, dev_qids))
    else:
        train = list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
        dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_ys, dev_ye, dev_x_str, dev_qids))
    # for dev_y_list (for multiple answers)
    if opt['lang'] == 'eng':
        dev_orig = pd.read_csv('data/squad6/dev.csv', encoding='utf8')
        dev_qid_list = dev_orig['id'].tolist()[:len(dev)]
        dev_ans_list = dev_orig['answers'].tolist()[:len(dev)]
        dev_ans_dict = {}
        for qid, ans in zip(dev_qid_list, dev_ans_list):
            dev_ans_dict[qid] = eval(ans)
    else:
        dev_ans_dict = {}
    return train, dev, dev_ans_dict, embedding, opt

def memReport():
    for obj in gc.get_objects():
        if torch.is_tensor(obj) or (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
            logger.info(type(obj), obj.size())

def cpuStats():
    logger.info(sys.version)
    logger.info(psutil.cpu_percent())
    logger.info(psutil.virtual_memory())  # physical memory usage
    pid = os.getpid()
    py = psutil.Process(pid)
    memoryUse = py.memory_info()[0] / 2. ** 30  # memory use in GB...I think
    logger.info('memory GB: {}'.format(memoryUse))

def write_result(file_path, predictions, dev_single_y_list, dev_y_list, score_list, opt):
    f = open(os.path.join(file_path, 'result.txt'), 'w')
    if opt['lang'] == 'eng':
        f.write('#{} {} {}\n'.format(len(predictions), len(dev_y_list), len(dev_single_y_list)))
        for pred, ans, single_ans in zip(predictions, dev_y_list, dev_single_y_list):
            f.write('\t'.join([pred, _normalize_answer(pred), ans, single_ans]))
            f.write('\n')
    else:
        logger.info('#{} {} {}'.format(len(predictions), len(dev_single_y_list), len(score_list)))
        try:
            for pred, ans, score in zip(predictions, dev_single_y_list, score_list):
                f.write('\t'.join([str(pred), str(_normalize_answer(pred)), str(ans), str(score)]))
                f.write('\n')
        except UnicodeEncodeError:
            logger.error('# UnicodeEncodeError!')
            pass
    return

def _exact_match(pred, answers):
    if pred is None or answers is None:
        return False
    pred = _normalize_answer(pred)
    for a in answers:
        if pred == _normalize_answer(a):
            return True
    return False

def _normalize_answer(s):
    def remove_articles(text):
        return re.sub(r'\b(a|an|the)\b', ' ', text)

    def white_space_fix(text):
        return ' '.join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return ''.join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))

def _f1_score(pred, answers):
    def _score(g_tokens, a_tokens):
        common = collections.Counter(g_tokens) & collections.Counter(a_tokens)
        num_same = sum(common.values())
        if num_same == 0:
            return 0
        precision = 1. * num_same / len(g_tokens)
        recall = 1. * num_same / len(a_tokens)
        f1 = (2 * precision * recall) / (precision + recall)
        return f1

    if pred is None or answers is None:
        return 0
    g_tokens = _normalize_answer(pred).split()
    scores = [_score(g_tokens, _normalize_answer(a).split()) for a in answers]
    return max(scores)

def score(pred, truth):
    assert len(pred) == len(truth)
    f1 = em = total = 0
    for p, t in zip(pred, truth):
        total += 1
        em += _exact_match(p, t)
        f1 += _f1_score(p, t)
    em = 100. * em / total
    f1 = 100. * f1 / total
    return em, f1

def evaluate(batches, model, dev_ans_dict, opt):
    predictions = []
    qid_list = []
    score_lst = []
    dev_y_list = []
    dev_single_y_list = []
    for batch in batches:
        pred, pred_score, _ = model.predict(batch)
        ans = model.get_answer(batch)
        # normalize prediction and answer
        new_pred = []
        new_ans = []
        for p in pred:
            p = p.replace(' \'s', '\'s')
            p = p.replace(' ,', ',')
            p = p.replace(' - ', '-')
            p = p.replace(' / ', '/')
            p = p.replace(' ( ', ' (')
            p = p.replace(' ) ', ') ')
            p = p.replace("do n't", "don't")
            p = p.replace('US$ ', 'US$')
            p = p.replace('5 k resolution', '5k resolution')
            new_pred.append(p)
        for a_list in ans:
            new_a_list = []
            for a in a_list:
                a = a.replace(' \'s', '\'s')
                a = a.replace(' ,', ',')
                a = a.replace(' - ', '-')
                a = a.replace(' / ', '/')
                a = a.replace(' ( ', ' (')
                a = a.replace(' ) ', ') ')
                a = a.replace("do n't", "don't")
                a = a.replace('US$ ', 'US$')
                a = a.replace('5 k resolution', '5k resolution')
                new_a_list.append(a)
            new_ans.append(new_a_list)
        pred = new_pred
        ans = new_ans
        # end
        predictions.extend(pred)
        dev_single_y_list.extend(ans)
        score_lst.extend(pred_score)	# pred_score
        #import pdb; pdb.set_trace()
        qids = batch[-1]
        qid_list.extend(qids)
        if opt['lang'] == 'eng':
            ans_list = [dev_ans_dict[qid] for qid in qids]
            dev_y_list.extend(ans_list)
    return predictions, dev_single_y_list, dev_y_list, score_lst

def lr_decay(optimizer, lr_decay):
    for param_group in optimizer.param_groups:
        param_group['lr'] *= lr_decay
    logger.info('[learning rate reduced by {}]'.format(lr_decay))
    return optimizer

def load_embedding(file_name, word2id_dic, vocab_size, wv_dim):
    """ Loads word vectors from word2vec embedding (key value1 value2 ...)
    """
    embedding = 0.01 * numpy.random.randn(vocab_size, wv_dim)
    with open(file_name, encoding='utf8') as f:
        count = 0
        for line in f:
            elems = line.split()
            token = elems[0]
            if token in word2id_dic:
                embedding[word2id_dic[token]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
            elif token.encode('utf8') in word2id_dic:
                embedding[word2id_dic[token.encode('utf8')]] = [float(v) for v in elems[-wv_dim:]]
                count += 1
        logger.info('Load word embedding: {}, {}, {}'.format(file_name, wv_dim, count))
    return embedding
