#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import json

import grpc
from google.protobuf import empty_pb2
from google.protobuf import json_format

from common.config import Config
from maum.brain.nlp import nlp_pb2
from maum.common import lang_pb2


class NlpClient:
    stub = None
    conf = Config()

    def __init__(self):
        remote = '127.0.0.1:{}'.format(self.conf.get('brain-ta.nlp.2.eng.port'))
        channel = grpc.insecure_channel(remote)
        self.stub = nlp_pb2.NaturalLanguageProcessingServiceStub(channel)

    def get_provider(self):
        ret = self.stub.GetProvider(empty_pb2.Empty())
        json_ret = json_format.MessageToJson(ret, True)

    def tokenize(self, my_text):
        in_text = nlp_pb2.InputText()
        in_text.text = my_text
        in_text.lang = lang_pb2.eng
        in_text.split_sentence = True
        in_text.use_tokenizer = True

        ret = self.stub.Analyze(in_text)
        # JSON text로 만들어낸다.
        json_text = json_format.MessageToJson(ret, True)
        final_result = list()
        doc = json.loads(json_text)
        for i in range(len(doc['sentences'])):
            for word in doc['sentences'][i]['words']:
                final_result.append(word['text'])
        return final_result

    def tokenize_sen(self, my_text):
        in_text = nlp_pb2.InputText()
        in_text.text = my_text
        in_text.lang = lang_pb2.eng
        in_text.split_sentence = True
        in_text.use_tokenizer = False

        ret = self.stub.Analyze(in_text)
        json_text = json_format.MessageToJson(ret, True)
        final_result = list()
        doc = json.loads(json_text)

        for i in range(len(doc['sentences'])):
            sen_unit = list()
            for word in doc['sentences'][i]['words']:
                sen_unit.append(word['text'])
            final_result.append(sen_unit)

        sentences = final_result
        words = my_text.split(" ")

        return_list = []
        w_idx = 0
        flag_word = ""
        for sent in sentences:
            return_syll = [ ]
            tokens = []
            t_idx = 0

            while t_idx < len(sent):
                token = sent[t_idx]

                if w_idx == len(words): break

                elif token == words[w_idx]:
                    tokens.append(token.lower())
                    return_syll.append(tokens)
                    tokens = []
                    w_idx += 1

                elif flag_word == words[w_idx]:
                    if len(tokens) is not 0: return_syll.append(tokens)
                    w_idx += 1
                    flag_word = ""
                    tokens = []
                    t_idx = t_idx -1

                else:
                    flag_word = flag_word + token
                    tokens.append(token.lower())
                    if t_idx == len(sent)-1:
                        return_syll.append(tokens)

                t_idx += 1
            return_list.append(return_syll)

        return return_list

def minds_nlp_pre(test_str):
    nlp_client = NlpClient()
    nlp_client.get_provider()
    return nlp_client.tokenize(test_str)

def minds_nlp(test_str):
    nlp_client = NlpClient()
    nlp_client.get_provider()
    return nlp_client.tokenize_sen(test_str)


if __name__ == '__main__':
    nlp_client = NlpClient()
    nlp_client.get_provider()
