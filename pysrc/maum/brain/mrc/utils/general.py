#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import logging
import io

def custom_logger(name, type='stream', fn=None, tqdm=False):
    fmt = '[{}|%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s'.format(name)
    fmt_date = '%Y-%m-%d_%T %Z'
    if type == 'stream':
        handler = logging.StreamHandler()
    elif type == 'file':
        handler = logging.FileHandler(filename=fn)
    else:
        logging.critical("Undefined logging type!: {}".format(type))
        logging.info("Only 'stream', 'file' types are available.")
        exit()   # TODO: exception handling?
    formatter = logging.Formatter(fmt, fmt_date)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)


class tqdm_to_logger(io.StringIO):
    # referencd: https://stackoverflow.com/questions/14897756/python-progress-bar-through-logging-module/41224909#41224909
    """
        Output stream for TQDM which will output to logger module instead of
        the StdOut.
    """
    logger = None
    level = None
    buf = ''
    def __init__(self,logger):
        super(tqdm_to_logger, self).__init__()
        self.logger = logger
        self.level = logger.level
    def write(self,buf):
        self.buf = buf.strip('\r\n\t ')
    def flush(self):
        self.logger.log(self.level, self.buf)
