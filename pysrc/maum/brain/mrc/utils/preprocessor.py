#! /usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import os, sys
import logging
from datetime import datetime
import ast
import shutil

from tqdm import tqdm
import msgpack as mp

from maum.brain.mrc.utils.prepro_util import PreproUtils
from maum.brain.mrc.utils.general import tqdm_to_logger

# import maum.brain.mrc.utils.prepro_util_eng as util_eng

s_util = PreproUtils()


def m_lst2lst(m_lst):
    lst = []
    for l in m_lst:
        for sent in l:
            for w in sent:
                lst.append(w)
    return lst


class Preprocessor:
    def __init__(self, model, lang, conf, uuid):
        self.logger = logging.getLogger('trainer')
        self.model = model
        self.lang = lang
        self.conf = conf
        self.uuid = uuid
        self.rsc_path = self.conf.get('mrc.resource.root')
        self.original_path = os.path.join(self.conf.get('mrc.original.dir'), lang,
                                          model)  # TODO: WHERE TO BE THE ORIGINAL FILES?
        self.ws_path = self.conf.get('mrc.trainer.workspace.dir')
        self.ws_path_proj = os.path.join(self.ws_path, self.uuid)

    # TODO: 원본 파일 위치 결정 필요(TEMP)
    def cp_original_files(self):
        if not os.path.exists(os.path.join(self.ws_path_proj, "data")):
            os.makedirs(os.path.join(self.ws_path_proj, "data"), exist_ok=True)
        for json_file in os.listdir(self.original_path):
            if "train" in json_file:
                shutil.copyfile(os.path.join(self.original_path, json_file),
                                os.path.join(self.ws_path_proj, "data", "train.json"))
            elif "dev" in json_file:
                shutil.copyfile(os.path.join(self.original_path, json_file),
                                os.path.join(self.ws_path_proj, "data", "dev.json"))
        self.logger.info("Workspace/data path: {}".format(os.path.join(self.ws_path_proj, "data")))
        self.logger.info("Files in workspace/data: {}".format(os.listdir(os.path.join(self.ws_path_proj, "data"))))

    def main(self):
        start0 = datetime.now()
        self.logger.info("Language: {}".format(self.lang))

        self.cp_original_files()

        if self.lang == "kor":
            voca_file = open(os.path.join(self.rsc_path, 'mrc-' + self.lang, 'kor_vectors_50.sp.vocab'), 'r',
                             encoding='cp949')
        elif self.lang == "eng":
            voca_file = open(os.path.join(self.rsc_path, 'mrc-' + self.lang, 'words.vocab'), 'r', encoding='cp949')
        vocab_dic, voca_list = s_util.load_vocab(voca_file)

        tr_data_dic = {}
        dev_data_dic = {}
        tqdm_out = tqdm_to_logger(self.logger)
        for data_type in ['dev', 'train']:
            self.logger.info("Time to begin: {}".format(data_type))
            f_name = os.path.join(self.ws_path_proj, 'data', '{}.json'.format(data_type))
            kdocs = s_util.kdoc_open(f_name)
            kdocs_data = kdocs['data']

            x_lst, q_lst, a_lst, x_str, q_str, qids = [], [], [], [], [], []

            if self.lang == "kor":
                a_start, a_end = None, None
                for doc in tqdm(kdocs_data, file=tqdm_out, mininterval=1, ):
                    for p in doc['paragraphs']:
                        mo_cont = m_lst2lst(ast.literal_eval(p['context']))
                        mo_cont.append('</s>')
                        mo_cont_i = s_util.make_idx_list(mo_cont, vocab_dic)

                        for qa in p['qas']:
                            qids.append(qa['id'])
                            que = ast.literal_eval(qa['question'])
                            mo_q = m_lst2lst(que)

                            q_lst.append(s_util.make_idx_list(mo_q, vocab_dic))
                            x_lst.append(mo_cont_i)
                            q_str.append(mo_q)
                            x_str.append(mo_cont)

                            for a in qa['answers']:
                                a_start = int(a['answer_start'])
                                a_end = int(a['answer_end']) - 1

                            if a_start is not None and a_end is not None:
                                a_lst.append([len(mo_cont) - 1, a_start, a_end])
                            else:
                                self.logger.critical("a_start: {}, a_end:{}".format(a_start, a_end))
                                # TODO: ERROR HANDLING???
                            if len(mo_cont) - 1 <= a_start or len(mo_cont) - 1 <= a_end:
                                self.logger.critical(
                                    "qid:{}, x_len:{}, a_start: {}, a_end:{}".format(qa['id'], len(mo_cont) - 1,
                                                                                     a_start, a_end))
                                raise RuntimeError("a >= 0 && a < x_classes")
            else:
                not_filtered = 0  # variable to count QA pair not containing an error.
                for doc in tqdm(kdocs_data):
                    for p in doc['paragraphs']:
                        mo_cont = util_eng.minds_nlp_pre(p['context'].lower())

                        mo_cont.append('</s>')
                        mo_cont_i = s_util.make_idx_list(mo_cont, vocab_dic)
                        for qa in p['qas']:

                            mo_q = util_eng.minds_nlp_pre(qa['question'].replace("?", "").lower())
                            mo_q_i = s_util.make_idx_list(mo_q, vocab_dic)

                            ''' 
                            The role of char_word_con is to match 
                            index of character with index of word
                            ex) 
                            p['context'] = The winner is brazil.
                            mo_cont = ['the','winner','is','brazil','.']
                            { 0: [0,2], 1: [4,9], 2: [11,12], 3: [14,19], 4:[20,20] }
                            '''

                            char_word_con = {}
                            index_of_word, index_of_char = 0, 0
                            for cont in mo_cont:
                                try:
                                    list_char_idx = []  # a list to be a value of char_word_con
                                    list_char_idx.append(index_of_char)
                                    len_word = len(cont)
                                    list_char_idx.append(index_of_char + len_word)
                                    char_word_con[index_of_word] = list_char_idx
                                    search_range = p['context'][char_word_con[index_of_word][1]:].lower()
                                    index_of_char = search_range.find(mo_cont[index_of_word + 1]) + \
                                                    char_word_con[index_of_word][1]
                                    index_of_word += 1
                                except:
                                    continue

                            for a in qa['answers']:
                                # ans is needed to find c_end (character_end for eng. data)
                                ans = a['text']

                                c_start = int(a['answer_start'])
                                c_end = c_start + len(ans)

                                a_start, a_end = 0, 0
                                for word_i, char_i in char_word_con.items():
                                    if char_i[0] == c_start:
                                        a_start = word_i

                                    if char_i[1] == c_end:
                                        a_end = word_i

                                if p['context'][c_start].lower() == mo_cont[a_start][0] and p['context'][
                                            c_end - 1].lower() == mo_cont[a_end][-1]:

                                    # This part is adjusted to handle the format of eng. squad data
                                    qids.append(qa['id'])
                                    a_lst.append([len(mo_cont) - 1, a_start, a_end])
                                    if len(mo_cont) - 1 <= a_start or len(mo_cont) - 1 <= a_end:
                                        self.logger.critical(
                                            "qid:{}, x_len:{}, a_start: {}, a_end:{}".format(qa['id'], len(mo_cont) - 1,
                                                                                             a_start, a_end))
                                        raise RuntimeError("a >= 0 && a < x_classes")

                                    q_lst.append(mo_q_i)
                                    x_lst.append(mo_cont_i)
                                    q_str.append(mo_q)
                                    x_str.append(mo_cont)

                                    not_filtered += 1

            self.logger.info("### total time: {}".format(str(datetime.now() - start0).split('.')[0]))
            if 'train' == data_type:
                tr_data_dic = {'x_lst': x_lst,
                               'q_lst': q_lst,
                               'a_lst': a_lst,
                               'x_str': x_str,
                               'q_str': q_str,
                               'qids': qids,
                               'voca_list': voca_list}
            else:
                dev_data_dic = {'x_lst': x_lst,
                                'q_lst': q_lst,
                                'a_lst': a_lst,
                                'x_str': x_str,
                                'q_str': q_str,
                                'qids': qids,
                                'voca_list': voca_list}

        train_xc, train_qc, dev_xc, dev_qc, char2id = \
            s_util.make_char_vocab(tr_data_dic['x_str'], tr_data_dic['q_str'],
                                   dev_data_dic['x_str'], dev_data_dic['q_str'])
        tr_data_dic['x_c_lst'] = train_xc
        tr_data_dic['q_c_lst'] = train_qc
        tr_data_dic['char2id'] = char2id
        dev_data_dic['x_c_lst'] = dev_xc
        dev_data_dic['q_c_lst'] = dev_qc
        dev_data_dic['char2id'] = char2id

        self.dic2msgpack(tr_data_dic, 'tr', self.ws_path)
        self.dic2msgpack(dev_data_dic, 'dev', self.ws_path)

    def dic2msgpack(self, dic, data_type, fw_path):
        with open(os.path.join(self.ws_path_proj, "data", "{}.msgpack".format(data_type)), 'wb') as f:
            mp.dump(dic, f, encoding='utf8')