#! /usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import logging
import json

class PreproUtils:

    def __init__(self):
        self.logger = logging.getLogger('trainer')

    def kdoc_open(self, f):
        doc_file = open(f, "r")
        kdoc = json.loads(doc_file.read())
        doc_file.close()
        return kdoc

    def load_vocab(self, voca_file):
        vocab_dic = {'<PAD>': 0, '<UNK>': 1}
        voca_list = [0, 1]
        for i, line in enumerate(voca_file.read().split('\n')):
            line = line.split("\n")[0].lower()
            vocab_dic[line] = i + 2
            voca_list.append(line)
        return vocab_dic, voca_list

    def make_idx_list(self, lnt_list, vocab_dic):
        idx_list = []
        for lemma in lnt_list:
            try: idx_list.append(vocab_dic[lemma])
            except:
                pos = lemma.split('/')[-1]
                try: idx_list.append(vocab_dic[pos])
                except: idx_list.append(vocab_dic['<UNK>'])
        return idx_list

    def make_char_vocab(self, train_x_str, train_q_str, dev_x_str, dev_q_str, char2id = None, data_dir=None):
        self.logger.info('make char data ...')
        if char2id is None:
            char2id = {'<PAD>':0, 'UNK':1}
            for d in train_x_str:
                for w in d:
                    for c in w:
                        if c not in char2id:
                            char2id[c] = len(char2id)
                            try:
                                if len(char2id) % 100 == 0:
                                    print(c, end=' ')
                            except: pass
        self.logger.info('char_vocab_size: {}'.format(len(char2id)))
        self.logger.info('make train_xc, train_qc, dev_xc, dev_qc ...')

        def make_cx(x_str):
            xc = []
            for d in x_str:
                w_list = []
                for w in d:
                    c_list = []
                    for c in w:
                        if c in char2id: c_list.append(char2id[c])
                        else: c_list.append(char2id['UNK'])
                    w_list.append(c_list)
                xc.append(w_list)
            return xc
        train_xc = make_cx(train_x_str)
        train_qc = make_cx(train_q_str)
        dev_xc = make_cx(dev_x_str)
        dev_qc = make_cx(dev_q_str)
        return train_xc, train_qc, dev_xc, dev_qc, char2id
