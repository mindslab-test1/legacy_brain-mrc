#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import os
import sys
import logging
import shutil

activator = os.path.join(os.environ['HOME'], ".virtualenvs/venv_mrc/bin/activate_this.py")
with open(activator) as f:
    exec(f.read(), {'__file__': activator})

import boto3

cur_path = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(cur_path, '..', '..', '..', '..'))
import maum.brain.mrc.utils.general as util

util.custom_logger('downloader', 'stream')
logger = logging.getLogger('downloader')

class TaResource:
    def __init__(self):
        # s3 bucket name
        self.bucket = "private.file.mindslab"
        self.s3 = boto3.client('s3')
        try:
            self.deploy = os.environ['MAUM_BUILD_DEPLOY'] == 'true'
            self.repo_root = os.environ['REPO_ROOT']
            self.outdir = os.path.join(self.repo_root, 'out')
            if not os.path.exists(self.outdir):
                os.makedirs(self.outdir)
        except:
            self.deploy = False
            self.repo_root = ''
            self.outdur = ''

    def touch_file(self, path):
        basedir = os.path.dirname(path)
        if not os.path.exists(basedir):
            os.makedirs(basedir)
        with open(path, 'a'):
            os.utime(path, None)

    def get_resources(self, target, version, tar_proc_dir, down_dir):
        if not os.path.isdir(down_dir):
            if os.path.isfile(down_dir):
                os.remove(down_dir)
            os.makedirs(down_dir)

        tar_proc_dir = os.path.join(tar_proc_dir, target)
        if not os.path.isdir(tar_proc_dir):
            if os.path.isfile(tar_proc_dir):
                os.remove(tar_proc_dir)
            os.makedirs(tar_proc_dir)
        down_file = 'minds-model-' + target + '-' + version + '.tar.gz'
        down_obj = 'resources/' + down_file
        down_tar = os.path.join(down_dir, down_file)
        if not os.path.isfile(down_tar):
            logger.info('Downloading {} model...'.format(target))
            self.s3.download_file(self.bucket, down_obj, down_tar)
        if not self.deploy:
            tar_target = os.path.join(tar_proc_dir, 'resources', target)
            tar_target_file = os.path.join(tar_target, '.version')
            if os.path.isfile(tar_target_file):
                logger.info('{} resources already exist at {}'.format(target, tar_target))
                return
            logger.info('Unpacking {} resources to {}'.format(target, tar_proc_dir))
            params = ['tar', 'xvf', down_tar, '-C', tar_proc_dir]
            cmd = ' '.join(params)
            os.system(cmd)
            logger.info('Done!')
        else:
            repo_tar = os.path.join(self.outdir, down_file)
            if not os.path.isfile(repo_tar):
                shutil.copyfile(down_tar, repo_tar)
                logger.info('{} is copied as {}'.format(target, repo_tar))

if __name__ == '__main__':
    receiver = TaResource()
    receiver.get_resources(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
