#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import os
import random
import logging
from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler
from common.config import Config

class Make_FD():
    conf = Config()
    conf.init('brain-mrc.conf')

    def __init__(self):
        self.log_dir = self.conf.get('mrc.tlo.dir')
        self.server_num = self.conf.get('mrc.tlo.server')

        self.logger = logging.getLogger('TLO')
        self.logger.setLevel(logging.INFO)
        self.fileHandler = ''

    def mkFD(self, flag=True, engine_name='MRC'):
        time = datetime.today().strftime("%Y%m%d%H%M%S%f")[:-4]
        file_path = os.path.join(self.log_dir, str(time)[:8])
        if not os.path.exists(file_path):
            os.makedirs(file_path, exist_ok=True)

        if int(time[11]) < 5:
            time = time[:11] + "0"
        else:
            time = time[:11] + "5"

        empty_filename = ".".join([engine_name, self.server_num, str(time), 'log'])
        with open(os.path.join(file_path, empty_filename), 'a') as f:
            pass
        if flag:
            self.fileHandler.__init__(os.path.join(file_path, empty_filename))
        else:
            self.fileHandler = logging.FileHandler(os.path.join(file_path, empty_filename))
            self.logger.addHandler(self.fileHandler)

    def run(self):
        print("TLO log option is ON...")
        self.mkFD(False)
        sched = BackgroundScheduler()
        sched.add_job(self.mkFD, 'cron', minute='*/5')
        sched.start()

def time_check(type=1):
    if type == 1:
        return datetime.today().strftime("%Y%m%d%H%M%S%f")[:-3]
    elif type == 0:
        return datetime.today().strftime("%Y%m%d%H%M%S%f")[:-3] + str(random.random())[2:6]
