#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import logging

from common.config import Config

class TLO:
    conf = Config()

    def __init__(self):
        self.logger = logging.getLogger('TLO')
        self.elem_list = ['QA_ENGINE', 'SEQ_ID', 'LOG_TIME', 'LOG_TYPE', 'SID', 'RESULT_CODE', 'REQ_TIME', 'RSP_TIME',
                          'CLIENT_IP', 'DEV_INFO', 'OS_INFO', 'NW_INFO', 'SVC_NAME', 'DEV_MODEL', 'CARRIER_TYPE',
                          'TR_ID', 'MSG_ID', 'FROM_SVC_NAME', 'TO_SVC_NAME', 'SVC_TYPE', 'DEV_TYPE', 'DEVICE_TOKEN']

    def write(self, meta):
        logging_list = list()
        for elem in self.elem_list:
            logging_list.append("{}={}".format(elem, str(meta[elem.lower()])))
        self.logger.info("|".join(logging_list))
