#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
import time
from concurrent import futures

import grpc

import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2_grpc as mrc_pb2_grpc
from common.config import Config
from maum.brain.mrc.run.run_server.mrc_server import MrcResolver

_ONE_DAY_IN_SECONDS = 60 * 60 * 24
logger = logging.getLogger('root')

def serve():
    logger.info('mrcd initializing...')
    conf = Config()
    conf.init('brain-mrc.conf')
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("mrc.runner.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("mrc.runner.front.timeout")))
    ]

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), None, data, None)
    mrc_pb2_grpc.add_MrcResolverServicer_to_server(MrcResolver(), server)

    port = conf.get('mrc.front.port')
    server.add_insecure_port('[::]:' + port)

    logger.info('mrcd starting at 0.0.0.0:' + port)
    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
