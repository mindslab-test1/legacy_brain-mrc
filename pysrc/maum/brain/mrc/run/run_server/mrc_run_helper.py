#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import logging
import fcntl

from common.config import Config
from maum.brain.mrc.run.tlo import mrc_runner_tlo_pb2 as mrcrn

logger = logging.getLogger('root')

class RunnerHelper:
    conf = Config()
    ws_dir = ''

    def __init__(self, uuid, lang, model):
        self.uuid = uuid
        self.lang = lang
        self.model = model
        self.ws_dir = self.workspace_path()
        self.proc_file = os.path.join(self.ws_dir, str(self.uuid) + '.proc')

    def workspace_path(self):
        p = self.conf.get('mrc.workspace.dir')
        if not os.path.exists(p):
            os.makedirs(p)
        return p

    def get_proc_file(self):
        return self.proc_file

    def load_proc(self):
        try:
            logger.debug("Load proc: {}".format(self.proc_file))
            f = open(self.proc_file, 'rb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_SH)
            proc = mrcrn.ServerStatus()
            proc.ParseFromString(f.read())
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            return proc
        except IOError as e:
            logger.critical('open failed: {} {} {}'.format(self.proc_file, e.errno, e.strerror))
            return None

    def save_proc(self, proc):
        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()

    def remove_proc(self):
        if os.path.exists(self.proc_file):
            f = open(self.proc_file, 'wb')
            fcntl.lockf(f.fileno(), fcntl.LOCK_EX)
            fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
            f.close()
            os.remove(self.proc_file)

    def init_proc(self):
        f = open(self.proc_file, 'wb')
        fcntl.lockf(f.fileno(), fcntl.LOCK_EX)

        proc = mrcrn.ServerStatus()
        proc.result = mrcrn.DNNBOT_SETMODEL_PREPARING

        f.write(proc.SerializeToString())
        fcntl.lockf(f.fileno(), fcntl.LOCK_UN)
        f.close()
