#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
import os
import socket
import subprocess
import sys
import signal
from uuid import uuid4

import grpc

import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2 as mrc_pb2
import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2_grpc as mrc_pb2_grpc
from maum.brain.mrc.run.run_server.mrc_run_helper import RunnerHelper
from common.config import Config

logger = logging.getLogger('root')

class MrcResolver(mrc_pb2_grpc.MrcResolverServicer):
    conf = Config()

    """
    Provide methods that implement MrcResolver
    """
    def __init__(self):
        self.mrc_servers = dict()
        self.base_port = int(self.conf.get('mrc.front.port'))

    def get_free_tcp_port(self):
        tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp.bind(('', 0))
        addr, port = tcp.getsockname()
        tcp.close()
        return port

    def child_command_line(self, model, uuid):
        port = self.get_free_tcp_port()
        exe_path = os.path.realpath(sys.argv[0])
        bin_path = os.path.dirname(exe_path)
        exe = os.path.join(bin_path, 'brain-mrc-proc')
        py = 'python'
        return py, [exe,
                    '-m', str(model.model),
                    '-l', str(model.lang),
                    '-p', str(port),
                    '-u', str(uuid),
                    #'-tlo'    # TODO: TLO option
                    ], str(port)

    def run_server(self, mrc_model, uuid):
        cmd, args, port = self.child_command_line(mrc_model, uuid)
        logger.debug("port: {}".format(port))
        env = os.environ.copy()
        env['PATH'] = env['PATH'] + ':/usr/local/cuda/bin'
        logger.info('\nBefore exec: {} {} {}\n'.format(cmd, args, env))
        try:
            cmd_list = [cmd]
            cmd_list += args
            p = subprocess.Popen(cmd_list)
            sub_pid = p.pid
            return port, sub_pid
        except OSError as e:
            logger.critical('execve failed {} {} {} {} {}'.format(cmd, args, env, e.errno, e.strerror))

    def SetModel(self, model, context):
        logger.info('Service {} is called'.format('SetModel'))
        uuid = uuid4()
        self.helper = RunnerHelper(uuid, model.lang, model.model)
        self.helper.init_proc()
        proc = self.helper.load_proc()

        proc.uuid = str(uuid)
        proc.model = model.model
        proc.lang = model.lang
        proc.server_address = '127.0.0.1'
        proc.port, sub_pid = self.run_server(model, uuid)
        self.mrc_servers[str(uuid)] = {'lang': model.lang, 'model': model.model, 'pid': sub_pid}
        self.helper.save_proc(proc)
        return proc

    def get_model(self, model_key):
        if model_key in self.mrc_servers:
            mrc_server_info = self.mrc_servers[model_key]
            helper = RunnerHelper(model_key, mrc_server_info['lang'], mrc_server_info['model'])
            proc = helper.load_proc()
            return proc
        else:
            return None

    def GetModel(self, key, context):
        logger.info('Service {} is called'.format('GetModel'))
        proc = self.get_model(str(key.uuid))
        if proc is not None:
            return proc
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('Server uuid not found')
            raise grpc.RpcError("Not found")

    def GetModels(self, empty, context):
        logger.info('Service {} is called'.format('GetModels'))
        model_list = mrc_pb2.ServerStatusList()
        for uuid in self.mrc_servers:
            proc = self.get_model(uuid)
            model_list.servers.extend([proc])
        return model_list

    def cancel(self, uuid):
        mrc_server_info = self.mrc_servers[uuid]
        helper = RunnerHelper(uuid, mrc_server_info['lang'], mrc_server_info['model'])
        proc = helper.load_proc()
        if proc.result == mrc_pb2.DNNBOT_SETMODEL_SUCCESS:
            logger.debug("sub_pid: {}".format(str(mrc_server_info['pid'])))
            os.kill(mrc_server_info['pid'], signal.SIGTERM)
            proc.result = mrc_pb2.DNNBOT_SETMODEL_CANCELED
            self.helper.save_proc(proc)
            result = True
        else:
            result = False
        return result, proc

    def Stop(self, key, context):
        logger.info('Service {} is called'.format('Stop'))
        logger.debug(self.mrc_servers)
        uuid = str(key.uuid)
        if uuid in self.mrc_servers:
            success, proc = self.cancel(uuid)
            if success:
                return proc
            else:
                context.set_code(grpc.StatusCode.INTERNAL)
                context.set_details('Server status is not currently running. Please check the uuid again.')
                raise grpc.RpcError('Internal error')
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('Server uuid not found')
            raise grpc.RpcError('Not found')
