#!/usr/bin/env python
# -*- coding:utf-8 -*-

import argparse
import collections
import json
import logging
import os
import random
import sys
import time
from concurrent import futures

import grpc
import torch
from google.protobuf.json_format import MessageToJson

sys.path.insert(0, os.path.join(os.environ['MAUM_ROOT'], 'lib', 'python'))
from common.config import Config
import maum.brain.mrc.utils.general as util
from maum.brain.mrc.core.model import DocReaderModel
from maum.brain.mrc.utils.prepro_util import PreproUtils
from maum.brain.mrc.utils.preprocessor import m_lst2lst

import maum.common.lang_pb2 as lang_pb2
from maum.brain.mrc.run.run_server.mrc_run_helper import RunnerHelper
import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2 as mrcrn
import maum.brain.mrc.run.tlo.mrc_runner_tlo_pb2_grpc as mrcrn_grpc

# TODO TLO
from maum.brain.mrc.tlo.logger import TLO
import maum.brain.mrc.tlo.util_tlo as u_tlo

# 정상
SUCCESS_STATUS_CODE = "20000000"
SUCCESS_STATUS_MESSAGE = "정상입니다."
# MRC Engine Down
MRC_STATUS_CODE_1 = "40000403"
MRC_STATUS_MESSAGE_1 = "MRC QA Engine 서버 내의 정답 인덱스 예측 단계에서 오류가 발생하였습니다."

class MrcModelLoader(mrcrn_grpc.MrcResolverServicer):
    conf = Config()
    conf.init('brain-mrc.conf')

    def __init__(self):
        # setup logger
        self.logger = self.set_logger()

        self.trained_dir = conf.get('mrc.trained.root')
        parser = argparse.ArgumentParser(
            description='Load a Document Reader model.'
        )
        ### grpc
        parser.add_argument('-m', '--model', nargs='?',
                            dest='model', required=True, help='Unique trainer id(uuid except sample best model)')
        parser.add_argument('-l', '--lang', nargs='?',
                            dest='lang', required=True, help='Language(kor, eng)')
        parser.add_argument('-p', '--port', nargs='?',
                            dest='port', required=True, help='Server port')
        parser.add_argument('-u', '--uuid', nargs='?',
                            dest='uuid', required=True, help='Server run unique ID')
        ### system
        parser.add_argument('-tlo', help='If you want to write TLO log, use it', action='store_true')  # TODO TLO
        parser.add_argument('--seed', type=int, default=411,
                            help='random seed for data shuffling, dropout, etc.')
        parser.add_argument('--cuda', type=bool,
                            default=torch.cuda.is_available(),
                            help='whether to use GPU acceleration.')
        parser.add_argument('-bs', '--batch_size', type=int, default=1)
        parser.add_argument('--max_len', type=int, default=50)
        parser.add_argument('--use_char', dest='use_char', action='store_true')
        parser.add_argument('--filter_sizes', default=[2,3,4,5,6], help='for char CNN')
        parser.add_argument('--question_merge', default='SELF_ATTEN', help='self_attn or avg')

        self.s_util = PreproUtils()
        self.args = parser.parse_args()
        self.helper = RunnerHelper(self.args.uuid, self.args.lang, self.args.model)
        self.proc = self.helper.load_proc()
        self.lang = lang_pb2.LangCode.Name(int(self.args.lang))

        # set TLO logger
        if self.args.tlo:
            # TODO TLO
            mk = u_tlo.Make_FD()
            mk.run()
            self.tlo_logger = TLO()

        # set random seed
        random.seed(self.args.seed)
        torch.manual_seed(self.args.seed)
        if self.args.cuda:
            torch.cuda.manual_seed(self.args.seed)
        # load model
        self.model, self.opt = self.load_model()

    def set_logger(self):
        conf = Config()
        self.log_dir = conf.get('logs.dir')
        os.environ['TZ'] = 'Asia/Seoul'
        time.tzset()
        log_dir = os.path.join(self.log_dir, 'mrc', 'server')
        if not os.path.exists(log_dir):
            os.makedirs(log_dir)
        logfile_name = os.path.join(log_dir, '{}_server.log'.format(time.strftime("%Y%m%d_%H%M%S")))
        util.custom_logger(name='server', type='file', fn=logfile_name)
        return logging.getLogger('server')

    def load_model(self):
        self.logger.info('[Server starts loading...]')
        self.logger.info('[Data loaded.]')
        self.logger.info('[loading previous model...]')
        if 'best' in self.args.model:
            checkpoint = torch.load(os.path.join(self.trained_dir, 'sample', 'mrc-{}'.format(self.lang), self.args.model+'.pt'))
        else:
            checkpoint = torch.load(os.path.join(self.trained_dir, self.args.model, self.args.model+'.pt'))
        opt = checkpoint['config']
        self.vocab_dic = opt['word2id']
        opt['max_len'] = self.args.max_len
        self.logger.info('max_len: {}'.format(opt['max_len']))
        opt['pretrained_words'] = False
        opt['cuda'] = self.args.cuda
        if type(opt['filter_sizes']) == str:
            opt['filter_sizes'] = [int(x) for x in opt['filter_sizes'].split(",")]
        state_dict = checkpoint['state_dict']
        model = DocReaderModel(opt, None, state_dict)
        if self.args.cuda:
            model.cuda()

        self.logger.info('[Server has been loaded...]')
        self.proc.result = mrcrn.DNNBOT_SETMODEL_SUCCESS
        self.helper.save_proc(self.proc)
        return model, opt

    # main
    def predict_main(self, morph_q, morph_passage_list):
        x_lst = []
        q_lst = []
        a_lst = [[None]] # fake data
        x_str = []
        q_str = []
        qids = [[None]] # fake data
        que = morph_q

        mo_q = m_lst2lst(que)
        for morph_passage in morph_passage_list:
            self.logger.debug("morph_passage: {}".format(morph_passage))
            mo_cont = m_lst2lst(morph_passage)  # flatten
            mo_cont.append('</s>')
            q_lst.append(self.s_util.make_idx_list(mo_q, self.vocab_dic))
            x_lst.append(self.s_util.make_idx_list(mo_cont, self.vocab_dic))
            q_str.append(mo_q)
            x_str.append(mo_cont)
        _, _, dev_xc, dev_qc, _ = self.s_util.make_char_vocab(x_str, q_str, x_str, q_str, self.opt['char2id'])

        # dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids
        dev = self.create_batch(x_lst, q_lst, dev_xc, dev_qc, a_lst*len(x_lst),
                                x_str, q_str, qids*len(x_lst))
        batches = BatchGen(dev, batch_size=self.args.batch_size, evaluation=True, gpu=self.args.cuda,
                           use_char=self.opt['use_char'], padding_idx=self.opt['padding_idx'],
                           char_padding_idx=self.opt['char_padding_idx'])
        predictions = []
        score_lst = []
        idx_tuple_list = []
        start_time = time.time()
        cnt = 1
        for batch in batches:
            pred, pred_score, idx_tuple = self.model.predict(batch)
            self.logger.info("prediction running time {}: {}".format(cnt, time.time()-start_time))
            # normalize prediction and answer
            new_pred = []
            for p in pred:
                p = p.replace(' \'s', '\'s')
                p = p.replace(' ,', ',')
                p = p.replace(' - ', '-')
                p = p.replace(' / ', '/')
                p = p.replace(' ( ', ' (')
                p = p.replace(' ) ', ') ')
                p = p.replace("do n't", "don't")
                p = p.replace('US$ ', 'US$')
                p = p.replace('5 k resolution', '5k resolution')
                new_pred.append(p)
            pred = new_pred
            # end
            predictions.extend(pred)
            score_lst.extend(pred_score)	# pred_score
            idx_tuple_list.append(idx_tuple)
            cnt += 1
        return predictions, score_lst, idx_tuple_list

    def map_original_idx(self, morph, original):
        flatten_morph = list()
        for s in morph:
            flatten_morph += s
        map_dic = dict()
        m_idx = 0
        o_idx = 0
        for m, o in zip(flatten_morph, original):
            max_o_idx = o_idx + len(o)
            if len(m) == 1:
                map_dic[m_idx] = (o_idx, o_idx+len(o))
                m_idx += 1
                o_idx += len(o) + 1
            else:
                pass_list = list()      # splitted morph ex. 나서+었
                start_from = 0
                fail = False  # beforehand
                for each_m in m:
                    splitted_m = each_m.rsplit("/", 1)[0]
                    location = o.find(splitted_m, start_from)
                    if location >= 0:
                        if fail:
                            o_idx += location
                            fail = False
                        map_dic[m_idx] = (o_idx, o_idx+len(splitted_m))
                        o_idx += len(splitted_m)
                        start_from = location
                    else:
                        pass_list.append((m_idx, m, start_from+o_idx, o))
                        fail = True
                    m_idx += 1
                pass_list_tmp = pass_list
                if len(pass_list) > 0:  # if there is a splitted morphs
                    for pass_item in pass_list:
                        # m_idx, m, start_position
                        backward_until = 1
                        while m_idx > (pass_item[0]+backward_until):
                            try:
                                #만약 그 다음 것도 찾지 못하면 하나 더 뒤로 감
                                map_dic[pass_item[0]] = (pass_item[2], map_dic[pass_item[0]+backward_until][0])
                                pass_list_tmp.remove(pass_item)
                                break
                            except KeyError:
                                backward_until += 1
                if len(pass_list_tmp) > 0:
                    for pass_item in pass_list_tmp:
                        map_dic[pass_item[0]] = (pass_item[2], max_o_idx)
                o_idx = max_o_idx
                o_idx += 1  # space
        # for </s> tag
        map_dic[m_idx] = (o_idx-1, o_idx-1)
        return map_dic

    def SendQuestion(self, request, context):
        FINAL_STATUS_CODE = ""
        FINAL_STATUS_MESSAGE = ""

        if self.args.tlo:  # TODO TLO
            request_time = u_tlo.time_check()
            meta = json.loads(MessageToJson(request.tlo, preserving_proto_field_name=True))
            meta['req_time'] = request_time
            meta['qa_engine'] = 'MRC'

        mrc_outputs = mrcrn.MrcOutputs()
        # morph analyzer
        self.logger.info("Received question: {}".format(eval(request.question)))
        morph_passage_list = list()
        original_passage_list = list()
        map_dic_list = list()
        for passage in request.passages:
            new_passage_original = " ".join(passage.words)
            self.logger.debug("Original passage: {}".format(new_passage_original))
            self.logger.debug("morph passage: {}".format(eval(passage.morp)))
            self.logger.debug("word_list: {}".format(passage.words))
            # append original passages
            original_passage_list.append(new_passage_original)
            # append morph passages
            morph_passage_list.append(eval(passage.morp))
            # append idx map dict
            map_dic = self.map_original_idx(eval(passage.morp), passage.words)
            map_dic_list.append(map_dic)
        self.logger.info("morph_passage_list: {}".format(morph_passage_list))

        # try to predict the answer
        try:
            prediction_list, loss_list, idx_t_list = self.predict_main(eval(request.question), morph_passage_list)
        except Exception as e:
            self.logger.critical(e)
            self.logger.critical(str(e))
            self.logger.critical(e.args)
            self.logger.critical(MRC_STATUS_MESSAGE_1)
            #FINAL_STATUS_CODE = MRC_STATUS_CODE_1  # TODO: define error code
            #FINAL_STATUS_MESSAGE = MRC_STATUS_MESSAGE_1
            #return None,

        for p, l, idx, ori_ctx, map_dic in zip(prediction_list, loss_list, idx_t_list, original_passage_list, map_dic_list):
            if p.startswith("|$|$|NO_ANSWER"):
                # can't find the answer in model
                self.logger.info("There's no answer(model)")
                mrc_outputs.answers.add(answer=p, prob=l)  # answer="|$|$|NO_ANSWER1|$|$|", prob=0
            else:
                self.logger.info("p: {}".format(p))
                self.logger.debug("l: {}".format(l))
                self.logger.info("idx: {}".format(idx))
                self.logger.info("ori_ctx: {}".format(ori_ctx))
                self.logger.debug("map_dic: {}".format(map_dic))
                s_idx = idx[0]
                e_idx = idx[1]
                real_answer = ori_ctx[map_dic[s_idx][0]:map_dic[e_idx][1]]
                self.logger.info("prediction: {} ({})".format(p, l))
                self.logger.info("real answer: {}".format(real_answer))
                if real_answer.strip() != "":
                    #mrc_outputs.answers.add(answer=p, prob=l)   # 형태소 단위 정답
                    mrc_outputs.answers.add(answer=real_answer, prob=l)  # 원본에서의 정답
                else:
                    self.logger.info("There's no answer(post-processing)")
                    mrc_outputs.answers.add(answer="|$|$|NO_ANSWER2|$|$|", prob=l)
        if self.args.tlo:
            meta['result_code'] = None  # TODO TLO
            meta['log_time'] = u_tlo.time_check()
            meta['rsp_time'] = u_tlo.time_check()
            self.tlo_logger.write(meta=meta)
        return mrc_outputs

    # exact_match , TF feature
    def make_feature(self, train_x, train_q, train_x_str, train_q_str):
        for x, q, xs, qs in zip(train_x, train_q, train_x_str, train_q_str): # using word idx
            if len(x) != len(xs) or len(q) != len(qs):
                self.logger.info(len(x), len(xs), len(q), len(qs))
                import pdb; pdb.set_trace()
        train_f = []
        #for x, q in zip(train_x, train_q): # using word idx
        for x, q in zip(train_x_str, train_q_str): # using word string
            f = [wid in q for wid in x]
            counter_ = collections.Counter(wid for wid in x)
            total = sum(counter_.values()) + 1e-5
            tf = [counter_[wid] / total for wid in x]
            train_f.append(list(zip(f, tf)))
        return train_f

    def create_batch(self, dev_x, dev_q, dev_xc, dev_qc, dev_y, dev_x_str, dev_q_str, dev_qids):
        dev_ys = [0]*len(dev_x)  # fake data
        dev_ye =[0]*len(dev_x)  # fake data
        dev_xf = self.make_feature(dev_x, dev_q, dev_x_str, dev_q_str)
        dev_qf = self.make_feature(dev_q, dev_x, dev_q_str, dev_x_str)
        self.logger.info('exact_match and TF features are generated.')
        # train, dev data
        if self.opt['use_char']:
            dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_xc, dev_qc, dev_ys, dev_ye, dev_x_str, dev_qids))
        else:
            dev = list(zip(dev_x, dev_xf, dev_q, dev_qf, dev_ys, dev_ye, dev_x_str, dev_qids))
        return dev


class BatchGen:
    def __init__(self, data, batch_size, gpu, evaluation=False, use_char=False, padding_idx=0, char_padding_idx=0):
        '''
        input:
            data - list of lists
            batch_size - int
        '''
        self.batch_size = batch_size
        self.evaluation = evaluation
        self.gpu = gpu
        self.use_char = use_char
        self.padding_idx = padding_idx
        self.char_padding_idx = char_padding_idx

        # shuffle
        if not evaluation:
            indices = list(range(len(data)))
            random.shuffle(indices)
            data = [data[i] for i in indices]
        # chunk into batches
        data = [data[i:i + batch_size] for i in range(0, len(data), batch_size)]
        self.data = data

    def __len__(self):
        return len(self.data)

    def __iter__(self):
        for batch in self.data:
            batch_size = len(batch)
            batch = list(zip(*batch))
            if self.use_char:
                assert len(batch) == 10
            else:
                assert len(batch) == 8

            # batch: list(zip(train_x, train_xf, train_q, train_qf, train_ys, train_ye, train_x_str, train_qids))
            # use_char: list(zip(train_x, train_xf, train_q, train_qf, train_xc, train_qc, train_ys, train_ye, train_x_str, train_qids))
            # train_x
            context_x_len = max(len(x) for x in batch[0])
            context_id = torch.LongTensor(batch_size, context_x_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[0]):
                context_id[i, :len(doc)] = torch.LongTensor(doc)

            # train_xf
            context_xf_len = max(len(x) for x in batch[1])
            feature_len = len(batch[1][0][0])
            context_feature = torch.Tensor(batch_size, context_xf_len, feature_len).fill_(0)
            # test - by leeck
            if context_x_len != context_xf_len:
                print('Error: context_x_len != context_xf_len')
                print('context_x_len=', context_x_len, 'context_xf_len=', context_xf_len)
                print(context_id.size(), context_feature.size())
                import pdb; pdb.set_trace()
            for i, doc in enumerate(batch[1]):
                for j, feature in enumerate(doc):
                    context_feature[i, j, :] = torch.Tensor(feature)

            # train_q
            question_len = max(len(x) for x in batch[2])
            question_id = torch.LongTensor(batch_size, question_len).fill_(self.padding_idx)
            for i, doc in enumerate(batch[2]):
                try: question_id[i, :len(doc)] = torch.LongTensor(doc)
                except: import pdb; pdb.set_trace()

            # train_qf
            context_qf_len = max(len(x) for x in batch[3])
            feature_len = len(batch[3][0][0])
            question_feature = torch.Tensor(batch_size, context_qf_len, feature_len).fill_(0)
            # test - by leeck
            if question_len != context_qf_len:
                print('Error: question_len != context_qf_len')
                print('question_len=', question_len, 'context_f_len=', context_qf_len)
                print(question_id.size(), question_feature.size())
                import pdb; pdb.set_trace()
            for i, q in enumerate(batch[3]):
                for j, feature in enumerate(q):
                    question_feature[i, j, :] = torch.Tensor(feature)

            # mask
            context_mask = torch.eq(context_id, self.padding_idx)
            question_mask = torch.eq(question_id, self.padding_idx)

            if self.use_char:
                # train_xc
                context_x_len = max(len(x) for x in batch[4])
                context_c_len = 6
                for x in batch[4]:
                    max_c_len = max(len(c) for c in x)
                    if max_c_len > context_c_len: context_c_len = max_c_len
                context_cid = torch.LongTensor(batch_size, context_x_len, context_c_len).fill_(self.char_padding_idx)
                for i, x in enumerate(batch[4]):
                    for j, w in enumerate(x):
                        context_cid[i, j, :len(w)] = torch.LongTensor(w)
                # train_qc
                question_len = max(len(q) for q in batch[5])
                question_c_len = 6
                for q in batch[5]:
                    max_c_len = max(len(c) for c in q)
                    if max_c_len > question_c_len: question_c_len = max_c_len
                question_cid = torch.LongTensor(batch_size, question_len, question_c_len).fill_(self.char_padding_idx)
                for i, q in enumerate(batch[5]):
                    for j, w in enumerate(q):
                        try: question_cid[i, j, :len(w)] = torch.LongTensor(w)
                        except: import pdb; pdb.set_trace()
                # mask
                context_cmask = torch.eq(context_cid, self.char_padding_idx)
                question_cmask = torch.eq(question_cid, self.char_padding_idx)

            # train_ys, train_ye
            y_s = torch.LongTensor(batch[-4])
            y_e = torch.LongTensor(batch[-3])

            # train_x_str
            text = list(batch[-2])

            # train_qids
            qid = list(batch[-1])

            if self.gpu:
                context_id = context_id.pin_memory()
                context_feature = context_feature.pin_memory()
                context_mask = context_mask.pin_memory()
                question_id = question_id.pin_memory()
                question_mask = question_mask.pin_memory()
                y_s = y_s.pin_memory()
                y_e = y_e.pin_memory()
                if self.use_char:
                    context_cid = context_cid.pin_memory()
                    question_cid = question_cid.pin_memory()
                    context_cmask = context_cmask.pin_memory()
                    question_cmask = question_cmask.pin_memory()
            if self.use_char:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, context_cid, context_cmask, question_cid, question_cmask, y_s, y_e, text, qid)
            else:
                yield (context_id, context_feature, context_mask, question_id, question_feature, question_mask, y_s, y_e, text, qid)

if __name__ == '__main__':
    conf = Config()
    conf.init('brain-mrc.conf')

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    loader = MrcModelLoader()
    logging.debug('Received port: {}'.format(loader.args.port))

    mrcrn_grpc.add_MrcResolverServicer_to_server(loader, server)
    server.add_insecure_port('[::]:{}'.format(loader.args.port))
    logging.info('Starting child with: {}'.format(sys.argv))
    server.start()

    while True:
        # Sleep forever, since `start` doesn't block
        time.sleep(1)
