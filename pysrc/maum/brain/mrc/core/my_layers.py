#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable

import maum.brain.mrc.core.cuda_functional as MF  # SRU


# ------------------------------------------------------------------------------
# Modules
# ------------------------------------------------------------------------------

# modified by leeck
# Origin: https://github.com/facebookresearch/ParlAI/tree/master/parlai/agents/drqa
class StackedBRNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers,
                 dropout_rate=0, dropout_output=False, rnn_type=nn.LSTM,
                 concat_layers=False, sum_layers=False, padding=False):
        super(StackedBRNN, self).__init__()
        self.padding = padding
        self.dropout_output = dropout_output
        self.dropout_rate = dropout_rate
        self.rnn_type = rnn_type
        self.num_layers = num_layers
        self.concat_layers = concat_layers
        # by leeck : for residual RNN
        self.sum_layers = sum_layers
        self.rnns = nn.ModuleList()
        for i in range(num_layers):
            input_size = input_size if i == 0 else 2 * hidden_size
            # by leeck
            if rnn_type == MF.SRUCell:
                self.rnns.append(MF.SRUCell(input_size, hidden_size,
                                            dropout=dropout_rate,
                                            rnn_dropout=dropout_rate,
                                            use_tanh=1,
                                            bidirectional=True))
            else:
                self.rnns.append(rnn_type(input_size, hidden_size,
                                          num_layers=1,
                                          bidirectional=True))

    def forward(self, x, x_mask):
        """Can choose to either handle or ignore variable length sequences.
        Always handle padding in eval.
        """
        # No padding necessary.
        if x_mask.data.sum() == 0:
            return self._forward_unpadded(x, x_mask)
        # by leeck
        if self.rnn_type == MF.SRUCell:
            return self._forward_unpadded(x, x_mask)
        # Pad if we care or if its during eval.
        if self.padding or not self.training:
            return self._forward_padded(x, x_mask)
        # We don't care.
        return self._forward_unpadded(x, x_mask)

    def _forward_unpadded(self, x, x_mask):
        """Faster encoding that ignores any padding."""
        # Transpose batch and sequence dims
        x = x.transpose(0, 1)

        # Encode all layers
        outputs = [x]
        for i in range(self.num_layers):
            rnn_input = outputs[-1]

            # Apply dropout to hidden input
            if self.rnn_type != MF.SRUCell: # by leeck
                if self.dropout_rate > 0:
                    rnn_input = F.dropout(rnn_input,
                                          p=self.dropout_rate,
                                          training=self.training)
            # Forward
            #rnn_output = self.rnns[i](rnn_input)[0]
            rnn_output, rnn_hn = self.rnns[i](rnn_input)
            outputs.append(rnn_output)

        # Concat hidden layers
        if self.concat_layers:
            output = torch.cat(outputs[1:], 2)
        elif self.sum_layers and self.num_layers > 1:
            # by leeck
            #output = outputs[1]
            #for i in range(2, self.num_layers + 1): output += outputs[i]
            output = outputs[1] + outputs[-1]
        else:
            output = outputs[-1]

        # Transpose back
        output = output.transpose(0, 1)

        # Dropout on output layer
        if self.dropout_output and self.dropout_rate > 0:
            output = F.dropout(output,
                               p=self.dropout_rate,
                               training=self.training)
        return output

    def _forward_padded(self, x, x_mask):
        """Slower (significantly), but more precise,
        encoding that handles padding."""
        # Compute sorted sequence lengths
        lengths = x_mask.data.eq(0).long().sum(1).squeeze()
        _, idx_sort = torch.sort(lengths, dim=0, descending=True)
        _, idx_unsort = torch.sort(idx_sort, dim=0)

        lengths = list(lengths[idx_sort])
        idx_sort = Variable(idx_sort)
        idx_unsort = Variable(idx_unsort)

        # Sort x
        x = x.index_select(0, idx_sort)

        # Transpose batch and sequence dims
        x = x.transpose(0, 1)

        # Pack it up
        rnn_input = nn.utils.rnn.pack_padded_sequence(x, lengths)

        # Encode all layers
        outputs = [rnn_input]
        for i in range(self.num_layers):
            rnn_input = outputs[-1]

            # Apply dropout to input
            if self.rnn_type != MF.SRUCell: # by leeck
                if self.dropout_rate > 0:
                    dropout_input = F.dropout(rnn_input.data,
                                              p=self.dropout_rate,
                                              training=self.training)
                    rnn_input = nn.utils.rnn.PackedSequence(dropout_input,
                                                            rnn_input.batch_sizes)
            outputs.append(self.rnns[i](rnn_input)[0])

        # Unpack everything
        for i, o in enumerate(outputs[1:], 1):
            outputs[i] = nn.utils.rnn.pad_packed_sequence(o)[0]

        # Concat hidden layers or take final
        if self.concat_layers:
            output = torch.cat(outputs[1:], 2)
        else:
            output = outputs[-1]

        # Transpose and unsort
        output = output.transpose(0, 1)
        output = output.index_select(0, idx_unsort)

        # Dropout on output layer
        if self.dropout_output and self.dropout_rate > 0:
            output = F.dropout(output,
                               p=self.dropout_rate,
                               training=self.training)
        return output


# by leeck
# added final hidden state
class StackedRNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers,
                 dropout_rate=0, dropout_output=False, rnn_type=nn.LSTM,
                 concat_layers=False, padding=False):
        super(StackedRNN, self).__init__()
        self.padding = padding
        self.dropout_output = dropout_output
        self.dropout_rate = dropout_rate
        self.rnn_type = rnn_type
        self.num_layers = num_layers
        self.concat_layers = concat_layers
        self.rnns = nn.ModuleList()
        for i in range(num_layers):
            input_size = input_size if i == 0 else hidden_size
            # by leeck
            if rnn_type == MF.SRUCell:
                self.rnns.append(MF.SRUCell(input_size, hidden_size,
                                            dropout=dropout_rate,
                                            rnn_dropout=dropout_rate,
                                            use_tanh=1,
                                            bidirectional=False))
            else:
                self.rnns.append(rnn_type(input_size, hidden_size,
                                          num_layers=1,
                                          bidirectional=False))

    def forward(self, x, x_mask):
        """Can choose to either handle or ignore variable length sequences.
        Always handle padding in eval.
        """
        # No padding necessary.
        if x_mask.data.sum() == 0:
            return self._forward_unpadded(x, x_mask)
        # by leeck
        if self.rnn_type == MF.SRUCell:
            return self._forward_unpadded(x, x_mask)
        # Pad if we care or if its during eval.
        if self.padding or not self.training:
            return self._forward_padded(x, x_mask)
        # We don't care.
        return self._forward_unpadded(x, x_mask)

    def _forward_unpadded(self, x, x_mask):
        """Faster encoding that ignores any padding."""
        # Transpose batch and sequence dims
        x = x.transpose(0, 1)

        # Encode all layers
        outputs = [x]
        for i in range(self.num_layers):
            rnn_input = outputs[-1]

            # Apply dropout to hidden input
            if self.rnn_type != MF.SRUCell: # by leeck
                if self.dropout_rate > 0:
                    rnn_input = F.dropout(rnn_input,
                                          p=self.dropout_rate,
                                          training=self.training)
            # Forward
            rnn_output = self.rnns[i](rnn_input)[0]
            outputs.append(rnn_output)

        # Concat hidden layers
        if self.concat_layers:
            output = torch.cat(outputs[1:], 2)
        else:
            output = outputs[-1]

        # Transpose back
        output = output.transpose(0, 1)

        # Dropout on output layer
        if self.dropout_output and self.dropout_rate > 0:
            output = F.dropout(output,
                               p=self.dropout_rate,
                               training=self.training)

        # final hidden state by leeck
        # output: batch * len_x * hidden
        hn = output[:, -1, :].clone()
        len_x = x_mask.data.eq(0).long().sum(1).squeeze()
        for i in range(output.size(0)):
            hn[i] = output[i, len_x[i] - 1, :]

        return output, hn

    def _forward_padded(self, x, x_mask):
        """Slower (significantly), but more precise,
        encoding that handles padding."""
        # Compute sorted sequence lengths
        lengths = x_mask.data.eq(0).long().sum(1).squeeze()
        _, idx_sort = torch.sort(lengths, dim=0, descending=True)
        _, idx_unsort = torch.sort(idx_sort, dim=0)

        lengths = list(lengths[idx_sort])
        idx_sort = Variable(idx_sort)
        idx_unsort = Variable(idx_unsort)

        # Sort x
        x = x.index_select(0, idx_sort)

        # Transpose batch and sequence dims
        x = x.transpose(0, 1)

        # Pack it up
        rnn_input = nn.utils.rnn.pack_padded_sequence(x, lengths)

        # Encode all layers
        outputs = [rnn_input]
        #hns = [] # by leeck
        for i in range(self.num_layers):
            rnn_input = outputs[-1]

            # Apply dropout to input
            if self.rnn_type != MF.SRUCell: # by leeck
                if self.dropout_rate > 0:
                    dropout_input = F.dropout(rnn_input.data,
                                              p=self.dropout_rate,
                                              training=self.training)
                    rnn_input = nn.utils.rnn.PackedSequence(dropout_input,
                                                            rnn_input.batch_sizes)
            outputs.append(self.rnns[i](rnn_input)[0])
            #output, hn = self.rnns[i](rnn_input)
            #outputs.append(output)
            #if self.rnn_type == nn.LSTM: hns.append(hn[0]) # (h_n, c_n)
            #else: hns.append(hn)

        # Unpack everything
        for i, o in enumerate(outputs[1:], 1):
            outputs[i] = nn.utils.rnn.pad_packed_sequence(o)[0]

        # Concat hidden layers or take final
        if self.concat_layers:
            output = torch.cat(outputs[1:], 2)
        else:
            output = outputs[-1]

        # Transpose and unsort
        output = output.transpose(0, 1)
        output = output.index_select(0, idx_unsort)

        # Dropout on output layer
        if self.dropout_output and self.dropout_rate > 0:
            output = F.dropout(output,
                               p=self.dropout_rate,
                               training=self.training)

        # final hidden state by leeck
        # output: batch * len_x * hidden
        # hn: batch * hidden
        hn = output[:, -1, :].clone()
        len_x = x_mask.data.eq(0).long().sum(1).squeeze()
        for i in range(output.size(0)):
            hn[i] = output[i, len_x[i] - 1, :]

        return output, hn


# Origin: https://github.com/facebookresearch/ParlAI/tree/master/parlai/agents/drqa
class SeqAttnMatch(nn.Module):
    """Given sequences X and Y, match sequence Y to each element in X.
    * o_i = sum(alpha_j * y_j) for i in X
    * alpha_j = softmax(y_j * x_i)
    """
    def __init__(self, input_size, identity=False):
        super(SeqAttnMatch, self).__init__()
        if not identity:
            self.linear = nn.Linear(input_size, input_size)
        else:
            self.linear = None

    def forward(self, x, y, y_mask):
        """Input shapes:
            x = batch * len1 * h
            y = batch * len2 * h
            y_mask = batch * len2
        Output shapes:
            matched_seq = batch * len1 * h
        """
        # Project vectors
        if self.linear:
            x_proj = self.linear(x.view(-1, x.size(2))).view(x.size())
            x_proj = F.relu(x_proj)
            y_proj = self.linear(y.view(-1, y.size(2))).view(y.size())
            y_proj = F.relu(y_proj)
        else:
            x_proj = x
            y_proj = y

        # Compute scores
        scores = x_proj.bmm(y_proj.transpose(2, 1))

        # Mask padding
        y_mask = y_mask.unsqueeze(1).expand(scores.size())
        scores.data.masked_fill_(y_mask.data, -float('inf'))

        # Normalize with softmax
        alpha_flat = F.softmax(scores.view(-1, y.size(1)))
        alpha = alpha_flat.view(-1, x.size(1), y.size(1))

        # Take weighted average
        matched_seq = alpha.bmm(y)
        return matched_seq


# Origin: https://github.com/facebookresearch/ParlAI/tree/master/parlai/agents/drqa
class BilinearSeqAttn(nn.Module):
    """A bilinear attention layer over a sequence X w.r.t y:
    * o_i = softmax(x_i'Wy) for x_i in X.

    Optionally don't normalize output weights.
    """
    def __init__(self, x_size, y_size, identity=False):
        super(BilinearSeqAttn, self).__init__()
        if not identity:
            self.linear = nn.Linear(y_size, x_size)
        else:
            self.linear = None

    def forward(self, x, y, x_mask):
        """
        x = batch * len * h1
        y = batch * h2
        x_mask = batch * len
        """
        Wy = self.linear(y) if self.linear is not None else y
        xWy = x.bmm(Wy.unsqueeze(2)).squeeze(2)
        if x.size(1) != x_mask.size(1):
            print(x.size(), x_mask.size())
            import pdb; pdb.set_trace()
        xWy.data.masked_fill_(x_mask.data, -float('inf'))
        if self.training:
            # In training we output log-softmax for NLL
            alpha = F.log_softmax(xWy)
        else:
            # ...Otherwise 0-1 probabilities
            alpha = F.softmax(xWy)
        return alpha


# Origin: https://github.com/facebookresearch/ParlAI/tree/master/parlai/agents/drqa
class LinearSeqAttn(nn.Module):
    """Self attention over a sequence:
    * o_i = softmax(Wx_i) for x_i in X.
    """
    def __init__(self, input_size):
        super(LinearSeqAttn, self).__init__()
        self.linear = nn.Linear(input_size, 1)

    def forward(self, x, x_mask):
        """
        x = batch * len * hdim
        x_mask = batch * len
        """
        # by leeck : 'input is not contiguous' runtime error
        x.contiguous()
        x_flat = x.view(-1, x.size(-1))
        scores = self.linear(x_flat).view(x.size(0), x.size(1))
        # by leeck
        try:
            scores.data.masked_fill_(x_mask.data, -float('inf'))
        except RuntimeError:
            import pdb; pdb.set_trace()
        if self.training:
            # In training we output log-softmax for NLL
            alpha = F.log_softmax(scores)
        else:
            # ...Otherwise 0-1 probabilities
            alpha = F.softmax(scores)
        return alpha


# for BIDAF model
class AttentionFlow(nn.Module):
    """Given sequences X (Context) and Q, return Query2Context and Context2Query attention.
    """
    def __init__(self, doc_hidden_size, question_hidden_size):
        super(AttentionFlow, self).__init__()
        if doc_hidden_size != question_hidden_size:
            self.linear_q_proj = nn.Linear(question_hidden_size, doc_hidden_size)
        else: self.linear_q_proj = None
        self.linear_p = nn.Linear(doc_hidden_size, 1)
        self.linear_q = nn.Linear(doc_hidden_size, 1)
        self.linear_pq = nn.Linear(doc_hidden_size, 1)

    def forward(self, x, q, x_mask, q_mask):
        """Input shapes:
            x = batch * len_x * h
            q = batch * len_q * h
            x_mask = batch * len_x (1 means padded)
            q_mask = batch * len_q (1 means padded)
        Output shapes:
            Query2Context = batch * (len_x) * h (len_x is expaned)
            Context2Query = batch * len_x * h
        """
        # Project vectors: question_hidden_size -> doc_hidden_size
        #print('x', x.size())
        #print('q', q.size())
        #print('x_mask', x_mask.size())
        #print('q_mask', q_mask.size())
        if self.linear_q_proj:
            q = self.linear_q_proj(q.view(-1, q.size(2))).view(q.size())
            q = F.relu(q)
        # by leeck : 'input is not contiguous' runtime error
        x.contiguous()
        q.contiguous()
        # x ((batch * len_x) * h) * linear_p (h * 1) -> batch * len_x
        p_proj = self.linear_p(x.view(-1, x.size(2))).view(x.size(0), x.size(1))
        # q ((batch * len_q) * h) * linear_q (h * 1) -> batch * len_q
        q_proj = self.linear_q(q.view(-1, q.size(2))).view(q.size(0), q.size(1))
        # mul: (batch * len_x * 1 * h) , (batch * 1 * len_q * h) -> batch * len_x * len_q * h
        x_expand = x.unsqueeze(2).expand(x.size(0), x.size(1), q.size(1), x.size(2))
        q_expand = q.unsqueeze(1).expand(q.size(0), x.size(1), q.size(1), x.size(2))
        pq = torch.mul(x_expand, q_expand)
        # pq ((batch * len_x * len_q) * h) * linear_pq (h * 1) -> batch * len_x * len_q
        pq_proj = self.linear_pq(pq.view(-1, pq.size(3))).view(pq.size(0), pq.size(1), pq.size(2))
        # s : batch * len_x * len_q
        s = pq_proj + p_proj.unsqueeze(2).expand(pq_proj.size()) + q_proj.unsqueeze(1).expand(pq_proj.size())
        # Mask padding for q
        q_mask = q_mask.unsqueeze(1).expand(s.size()) # batch * len_x * len_q
        s.data.masked_fill_(q_mask.data, -float('inf'))
        # s_tj -> c2q_attent ((batch * len_x) * len_q)
        c2q_attent_flat = F.softmax(s.view(-1, s.size(2)))
        c2q_attent = c2q_attent_flat.view(s.size())
        # bmm: c2q_attent (batch * len_x * len_q) , q (batch * len_q * h) -> batch * len_x * h
        c2q = c2q_attent.bmm(q)
        # Mask padding for x
        x_mask = x_mask.unsqueeze(2).expand(s.size()) # batch * len_x * len_q
        s.data.masked_fill_(x_mask.data, -float('inf'))
        # s_tj -> max_s_t (batch * len_x)
        max_s_t, _ = torch.max(s, 2)
        # max_s_t -> q2c_attent (batch * len_x)
        q2c_attent = F.softmax(max_s_t)
        # bmm: q2c_attent (batch * 1 * len_x) , x (batch * len_x * h) -> batch * 1 * h
        q2c = q2c_attent.unsqueeze(1).bmm(x)
        q2c = q2c.expand(x.size()) # batch * len_x * h
        return q2c, c2q


# for R-Net model
# using dot product for attention
class SelfMatchingAttention(nn.Module):
    """Given sequences X, match sequence X to each element in X.
    * o_i = sum(alpha_j * x_j) for i in X
    * alpha_j = softmax(FC(W1 x_j, W2 x_i))
    """
    def __init__(self, input_size, attent_type='dot'):
        super(SelfMatchingAttention, self).__init__()
        self.attent_type = attent_type
        if attent_type == 'dot':
            pass
        elif attent_type == 'bilinear':
            self.linear = nn.Linear(input_size, input_size)
        else:
            raise RuntimeError(attent_type)

    def forward(self, x, x_mask):
        """Input shapes:
            x = batch * len * h
            x_mask = batch * len
        Output shapes:
            matched_seq = batch * len * h
        """
        # Compute scores
        if self.attent_type == 'dot':
            # bmm: (batch * len * h) , (batch * h * len) -> batch * len * len
            scores = x.bmm(x.transpose(2, 1))
        elif self.attent_type == 'bilinear':
            Wx = self.linear(x.view(-1, x.size(2))).view(x.size())
            scores = x.bmm(Wx.transpose(2, 1))
        else:
            raise RuntimeError(attent_type)

        # Mask padding
        x_mask = x_mask.unsqueeze(1).expand(scores.size())
        scores.data.masked_fill_(x_mask.data, -float('inf'))

        # Normalize with softmax
        alpha_flat = F.softmax(scores.view(-1, scores.size(2)))
        alpha = alpha_flat.view(scores.size())

        # Take weighted average
        # bmm: batch * len * len , batch * len * h -> batch * len * h
        matched_seq = alpha.bmm(x)
        return matched_seq


# modified by leeck
# Yoon Kim's CNN from https://github.com/Shawn1993/cnn-text-classification-pytorch/blob/master/model.py
class CNN_Text(nn.Module):
    """A CNN model for text"""

    def __init__(self, input_size, output_size, filter_sizes=[3,4,5],
            num_filters=100, dropout_rate=0.2):
        super(CNN_Text, self).__init__()
        Ci = 1
        self.convs1 = nn.ModuleList([nn.Conv2d(Ci, num_filters, (K, input_size)) for K in filter_sizes])
        self.dropout = nn.Dropout(dropout_rate)
        self.fc1 = nn.Linear(len(filter_sizes)*num_filters, output_size)

    def forward(self, x):
        """
        input x: (batch, len_x, input_size)
        return : (batch, output_size)
        """
        # expand x to [batch, 1, len_x(H), input_size]
        x = x.unsqueeze(1)
        # (batch, 1, len_x, input_size) -> (batch, Co, H, 1) -> (batch, Co, H)
        # [(batch, Co, H), ...] * len(filter_sizes)
        x = [F.relu(conv(x)).squeeze(3) for conv in self.convs1]
        #[(batch, Co), ...] * len(filter_sizes)
        x = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in x]
        # (batch, len(filter_sizes) * Co)
        x = torch.cat(x, 1)
        x = self.dropout(x)
        # (batch, output_size)
        logit = self.fc1(x)
        return logit


# ------------------------------------------------------------------------------
# Functional
# ------------------------------------------------------------------------------


def uniform_weights(x, x_mask):
    """Return uniform weights over non-masked input."""
    alpha = Variable(torch.ones(x.size(0), x.size(1)))
    if x.data.is_cuda:
        alpha = alpha.cuda()
    alpha = alpha * x_mask.eq(0).float()
    # by leeck
    #alpha = alpha / alpha.sum(1).expand(alpha.size())
    alpha = alpha / alpha.sum(1).unsqueeze(1).expand(alpha.size())
    return alpha


def weighted_avg(x, weights):
    """x = batch * len * d
    weights = batch * len
    """
    return weights.unsqueeze(1).bmm(x).squeeze(1)
