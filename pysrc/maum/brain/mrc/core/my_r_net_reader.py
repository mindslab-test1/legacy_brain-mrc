#!/usr/bin/env python3.5
# -*- coding:utf-8 -*-

import torch
import torch.nn as nn

import maum.brain.mrc.core.my_layers as my_layers
import maum.brain.mrc.core.cuda_functional as MF  # SRU

class R_NetReader(nn.Module):
    """Network for the Document Reader module of R-Net."""
    #RNN_TYPES = {'lstm': nn.LSTM, 'gru': nn.GRU, 'rnn': nn.RNN}
    RNN_TYPES = {'lstm': nn.LSTM, 'gru': nn.GRU, 'rnn': nn.RNN, 'sru': MF.SRUCell}

    def __init__(self, opt, padding_idx=0, embedding=None):
        super(R_NetReader, self).__init__()
        # Store config
        self.opt = opt

        # Word embeddings
        if opt['pretrained_words']:
            assert embedding is not None
            self.embedding = nn.Embedding(embedding.size(0),
                                          embedding.size(1),
                                          padding_idx=padding_idx)
            self.embedding.weight.data = embedding
            if opt['fix_embeddings']:
                assert opt['tune_partial'] == 0
                for p in self.embedding.parameters():
                    p.requires_grad = False
            elif opt['tune_partial'] > 0:
                assert opt['tune_partial'] + 2 < embedding.size(0)
                fixed_embedding = embedding[opt['tune_partial'] + 2:]
                self.register_buffer('fixed_embedding', fixed_embedding)
                self.fixed_embedding = fixed_embedding
        else:  # random initialized
            self.embedding = nn.Embedding(opt['vocab_size'],
                                          opt['embedding_dim'],
                                          padding_idx=padding_idx)

        if opt['use_char']:
                self.embedding4char = nn.Embedding(opt['char_vocab_size'],
                                                   opt['char_embed_dim'],
                                                   padding_idx=opt['char_padding_idx'])
                self.char_cnn = my_layers.CNN_Text(
                    input_size=opt['char_embed_dim'],
                    output_size=opt['char_embed_dim2'],
                    filter_sizes=[int(x) for x in opt['filter_sizes'].split(',')],
                    num_filters=opt['num_filters'],
                    dropout_rate=opt['dropout_rnn'],
                )

        # Projection for attention weighted question
        if opt['use_qemb']:
            self.qemb_match = my_layers.SeqAttnMatch(opt['embedding_dim'])
        if opt['use_qemb2']:
            self.qemb_match2 = my_layers.SeqAttnMatch(opt['embedding_dim'])

        # Input size to RNN: word emb + question emb + manual features
        doc_input_size = opt['embedding_dim']
        if opt['use_char']:
            doc_input_size += opt['char_embed_dim2']
        if opt['exact_match']:
            doc_input_size += opt['num_features']
        if opt['use_qemb']:
            doc_input_size += opt['embedding_dim']
        # input size to RNN for question
        ques_input_size = opt['embedding_dim']
        if opt['use_char']:
            ques_input_size += opt['char_embed_dim2']
        if opt['exact_match2']:
            ques_input_size += opt['num_features']
        if opt['use_qemb2']:
            ques_input_size += opt['embedding_dim']

        # RNN document encoder
        self.doc_rnn = my_layers.StackedBRNN(
            input_size=doc_input_size,
            hidden_size=opt['hidden_size'],
            num_layers=opt['doc_layers'],
            dropout_rate=opt['dropout_rnn'],
            dropout_output=opt['dropout_rnn_output'],
            #concat_layers=opt['concat_rnn_layers'],
            concat_layers=False,
            sum_layers=opt['sum_rnn_layers'],
            rnn_type=self.RNN_TYPES[opt['rnn_type']],
            padding=opt['rnn_padding'],
        )

        # RNN question encoder
        self.question_rnn = my_layers.StackedBRNN(
            input_size=ques_input_size,
            hidden_size=opt['hidden_size'],
            num_layers=opt['question_layers'],
            dropout_rate=opt['dropout_rnn'],
            dropout_output=opt['dropout_rnn_output'],
            #concat_layers=opt['concat_rnn_layers'],
            concat_layers=False,
            sum_layers=opt['sum_rnn_layers'],
            rnn_type=self.RNN_TYPES[opt['rnn_type']],
            padding=opt['rnn_padding'],
        )

        # Output sizes of rnn encoders
        doc_hidden_size = 2 * opt['hidden_size']
        question_hidden_size = 2 * opt['hidden_size']
        if opt['concat_rnn_layers']:
            doc_hidden_size *= opt['doc_layers']
            question_hidden_size *= opt['question_layers']

        # Question merging
        if opt['question_merge'] not in ['avg', 'self_attn']:
            raise NotImplementedError('question_merge = %s' % opt['question_merge'])
        if opt['question_merge'] == 'self_attn':
            self.self_attn = my_layers.LinearSeqAttn(question_hidden_size)

        # Attention Flow layer - by leeck
        self.attention_flow = my_layers.AttentionFlow(doc_hidden_size, question_hidden_size)

        # Modeling layer (RNN)
        #print('doc_hidden_size', doc_hidden_size)
        self.modeling_rnn = my_layers.StackedBRNN(
            #input_size=4 * doc_hidden_size, # [doc_hiddens, c2q, mul_h_c2q, mul_h_q2c]
            input_size=3 * doc_hidden_size, # [doc_hiddens, c2q, mul_h_c2q]
            hidden_size=int(doc_hidden_size / 2),
            num_layers=opt['modeling_layers'],
            dropout_rate=opt['dropout_rnn'],
            dropout_output=opt['dropout_rnn_output'],
            concat_layers=False,
            sum_layers=opt['sum_rnn_layers'],
            rnn_type=self.RNN_TYPES[opt['rnn_type']],
            padding=opt['rnn_padding'],
        )

        # Self-Matching Attention layer - by leeck
        self.self_matching = my_layers.SelfMatchingAttention(doc_hidden_size)

        # Modeling2 layer (RNN)
        #print('doc_hidden_size', doc_hidden_size)
        self.modeling2_rnn = my_layers.StackedBRNN(
            input_size=5 * doc_hidden_size, # [G, M, S], G=[doc_hiddens, c2q, mul_h_c2q]
            hidden_size=int(doc_hidden_size / 2),
            num_layers=opt['modeling2_layers'],
            dropout_rate=opt['dropout_rnn'],
            dropout_output=opt['dropout_rnn_output'],
            concat_layers=False,
            sum_layers=opt['sum_rnn_layers'],
            rnn_type=self.RNN_TYPES[opt['rnn_type']],
            padding=opt['rnn_padding'],
        )

        # Linear attention for span start/end
        self.start_attn = my_layers.LinearSeqAttn(6 * doc_hidden_size) # [G, M, S, M2]
        self.end_attn = my_layers.LinearSeqAttn(6 * doc_hidden_size)

    #def forward(self, x1, x1_f, x1_mask, x2, x2_f, x2_mask):
    def forward(self, x1, x1_f, x1_mask, x2, x2_f, x2_mask, xc1=None, xc1_mask=None, xc2=None, xc2_mask=None):
        """Inputs:
        x1 = document word indices             [batch * len_d]
        x1_f = document word features indices  [batch * len_d * nfeat]
        x1_mask = document padding mask        [batch * len_d]
        x2 = question word indices             [batch * len_q]
        x2_mask = question padding mask        [batch * len_q]
        xc1 = document char indices            [batch * len_d * len_c]
        xc1_mask = document char padding mask  [batch * len_d * len_c]
        xc2 = question char indices            [batch * len_q * len_c]
        xc2_mask = question char padding mask  [batch * len_q * len_c]
        """
        # Embed both document and question
        x1_emb = self.embedding(x1)
        x2_emb = self.embedding(x2)
        if self.opt['use_char']:
            xc1_emb = self.embedding4char(xc1.view(-1, xc1.size(2))).view(xc1.size(0), xc1.size(1), xc1.size(2), -1)
            xc2_emb = self.embedding4char(xc2.view(-1, xc2.size(2))).view(xc2.size(0), xc2.size(1), xc2.size(2), -1)

        # Dropout on embeddings
        if self.opt['dropout_emb'] > 0:
            x1_emb = nn.functional.dropout(x1_emb, p=self.opt['dropout_emb'],
                                           training=self.training)
            x2_emb = nn.functional.dropout(x2_emb, p=self.opt['dropout_emb'],
                                           training=self.training)
            if self.opt['use_char']:
                xc1_emb = nn.functional.dropout(xc1_emb, p=self.opt['dropout_emb'], training=self.training)
                xc2_emb = nn.functional.dropout(xc2_emb, p=self.opt['dropout_emb'], training=self.training)

        # test by leeck
        #print(x1_f)
        drnn_input_list = [x1_emb]
        if self.opt['use_char']:
            xc1_emb2 = self.char_cnn(xc1_emb.view(-1, xc1_emb.size(2), xc1_emb.size(3)))
            drnn_input_list.append(xc1_emb2.view(xc1.size(0), xc1.size(1), -1))
        if self.opt['exact_match']:
            drnn_input_list.append(x1_f)
        # Add attention-weighted question representation
        if self.opt['use_qemb']:
            x2_weighted_emb = self.qemb_match(x1_emb, x2_emb, x2_mask)
            drnn_input_list.append(x2_weighted_emb)
        drnn_input = torch.cat(drnn_input_list, 2)
        # Encode document with RNN
        doc_hiddens = self.doc_rnn(drnn_input, x1_mask)

        # Encode question with RNN
        ques_input_list = [x2_emb]
        if self.opt['use_char']:
            xc2_emb2 = self.char_cnn(xc2_emb.view(-1, xc2_emb.size(2), xc2_emb.size(3)))
            ques_input_list.append(xc2_emb2.view(xc2.size(0), xc2.size(1), -1))
        if self.opt['exact_match2']:
            ques_input_list.append(x2_f)
        if self.opt['use_qemb2']:
            x1_weighted_emb = self.qemb_match2(x2_emb, x1_emb, x1_mask)
            ques_input_list.append(x1_weighted_emb)
        ques_input = torch.cat(ques_input_list, 2)
        question_hiddens = self.question_rnn(ques_input, x2_mask)
        # question merge
        '''
        if self.opt['question_merge'] == 'avg':
            q_merge_weights = my_layers.uniform_weights(question_hiddens, x2_mask)
        elif self.opt['question_merge'] == 'self_attn':
            q_merge_weights = self.self_attn(question_hiddens, x2_mask)
            if self.training: # for F.log_softmax
                q_merge_weights = q_merge_weights.exp()
        question_hidden = my_layers.weighted_avg(question_hiddens, q_merge_weights)
        '''

        # Attention Flow layer - by leeck
        q2c, c2q = self.attention_flow(doc_hiddens, question_hiddens, x1_mask, x2_mask)
        mul_h_c2q = torch.mul(doc_hiddens, c2q)
        #mul_h_q2c = torch.mul(doc_hiddens, q2c)
        #G = torch.cat([doc_hiddens, c2q, mul_h_c2q, mul_h_q2c], 2)
        G = torch.cat([doc_hiddens, c2q, mul_h_c2q], 2)

        # Modeling layer (RNN)
        M = self.modeling_rnn(G, x1_mask)
        #G_M = torch.cat([G, M], 2)

        # Self-Matching Attention layer - by leeck
        S = self.self_matching(M, x1_mask)
        G_M_S = torch.cat([G, M, S], 2)

        # Modeling2 layer (RNN)
        M2 = self.modeling2_rnn(G_M_S, x1_mask)
        G_M_S_M2 = torch.cat([G_M_S, M2], 2)

        # Predict start and end positions
        # linear attention
        start_scores = self.start_attn(G_M_S_M2, x1_mask)
        end_scores = self.end_attn(G_M_S_M2, x1_mask)
        # bilinear attention
        #start_scores = self.start_attn(G_M, question_hidden, x1_mask)
        #end_scores = self.end_attn(G_M, question_hidden, x1_mask)
        return start_scores, end_scores
