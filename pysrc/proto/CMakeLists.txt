cmake_minimum_required(VERSION 2.8.12)

project(proto_fake C)

set(OUT proto_fake)

set(PROTOBUF_GENERATE_CPP_APPEND_PATH "FALSE")
find_package(Protobuf REQUIRED
  paths ${CMAKE_INSTALL_PREFIX})
include(grpc)

set(PROTOBUF_IMPORT_DIRS
  ${CMAKE_HOME_DIRECTORY}/proto
  ${CMAKE_INSTALL_PREFIX}/include)

set(PROTO_SRCS
  ${CMAKE_HOME_DIRECTORY}/proto/maum/brain/mrc/run/mrc_runner.proto
  ${CMAKE_HOME_DIRECTORY}/proto/maum/brain/mrc/run/tlo/mrc_runner_tlo.proto
  ${CMAKE_INSTALL_PREFIX}/include/maum/common/lang.proto)

protobuf_generate_grpc_python(PB_SRCS ${PROTO_SRCS})

add_executable(${OUT}
  ._fake.c
  ${PB_SRCS})

install(DIRECTORY ./maum ./google
  DESTINATION lib/python
  FILES_MATCHING PATTERN "*.py")
