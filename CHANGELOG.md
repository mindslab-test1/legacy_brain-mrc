# [1.0.0](https://github.com/mindslab-ai/brain-mrc-train/compare/38f7ec2...v1.0.0)(2017-12-21)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
 MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->


### Bug Fixes

- **run(answer)**: 모델에서 정답을 추출하지 못한 경우 정해진 코드를 리턴하도록 기능 추가
([#21](https://github.com/mindslab-ai/minds-mrc/issues/26))([1a3c6d3](https://github.com/mindslab-ai/minds-mrc/commit/1a3c6d3920961e173de2eeb7d893ef4c909aa03e))
- **run(answer)**: 원본 문서에 띄어쓰기가 제대로 안 된 경우 출력 정답의 인덱스가 밀리는 현상
([#43](https://github.com/mindslab-ai/minds-mrc/issues/43))([5635719](https://github.com/mindslab-ai/minds-mrc/commit/5635719cf9871aba02f018b355e30827354dd45f))
- **run(prediction)**, **train**: SRU 스크립트에서 TypeError: 'float' object cannot be interpreted as an integer
([#46](https://github.com/mindslab-ai/minds-mrc/issues/46))([9146a1e](https://github.com/mindslab-ai/minds-mrc/commit/9146a1e06a7f11fa11a5886c41bdcfdbc94aac97))
- **run(prediction)**: 질문에 대한 답을 예측할 때 발생하는 RuntimeError: `Output size is too small`
([#47](https://github.com/mindslab-ai/minds-mrc/issues/47))([97b52e7](https://github.com/mindslab-ai/minds-mrc/commit/97b52e73573218053c55a8083c242d7e98b4a005))
- **common**: trainer에서 실제 학습을 수행해야 하는 child process에서 학습이 수행되지 않는 문제
([#89](https://github.com/mindslab-ai/brain-mrc-train/issues/89))([e138e08](https://github.com/mindslab-ai/brain-mrc-train/commit/e138e08ffa595fc416195bb5e08f80c11908697c))
- **build**: libmaum이 설치되지 않은 상태에서 빌드를 시도할 경우 루프를 도는 문제
([#106](https://github.com/mindslab-ai/brain-mrc-train/issues/106))([64fd516](https://github.com/mindslab-ai/brain-mrc-train/commit/64fd5162afabc3f57104ae213cd413458321c590))
- **build**: grpc python 최신 버전 빌드에서 pb2.py 와 pb2.grpc.py가 분리되는 문제
([#125](https://github.com/mindslab-ai/brain-mrc-train/issues/125))([9334cfc](https://github.com/mindslab-ai/brain-mrc-train/commit/9334cfccee72aeac61609bd770223815d91b3723))
- **train**: attention flow 버그 수정
([#121](https://github.com/mindslab-ai/brain-mrc-train/issues/121))([652422b](https://github.com/mindslab-ai/brain-mrc-train/pull/121/commits/652422b55621ef59b60bbdd1277bd528c7300405))


### Features

- **run(prediction)**: 기 학습된 모델을 불러 와 질의응답하는 기능 grpc 서버화
([#18](https://github.com/mindslab-ai/minds-mrc/issues/18))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **run(prediction)**:  개별 질문/단락이 들어왔을 때 정답을 리턴하는 기능(상동)
- **run(answer)**: 최종 정답을 형태소가 아닌 원본 문서에서 찾도록 하는 기능
([#19](https://github.com/mindslab-ai/minds-mrc/issues/19))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **pre-pro(ko)**: 한국어 데이터를 전처리하여 학습을 할 수 있는 형태로 변형하는 기능
- **pre-pro(en)**: 영어 데이터를 전처리하여 학습을 할 수 있는 형태로 변형하는 기능
([#3](https://github.com/mindslab-ai/minds-mrc/issues/3))([#61](https://github.com/mindslab-ai/minds-mrc/issues/61))
([e24c8c9](https://github.com/mindslab-ai/minds-mrc/commit/e24c8c96f74a8c320a7ed6ed1b61f76fab7e7f76))
- **run(common)**: TLO 로깅 기능(실행 과정에서 옵션을 주어 끄고 켤 수 있도록 함)
([#60](https://github.com/mindslab-ai/minds-mrc/issues/60))([6460307](https://github.com/mindslab-ai/minds-mrc/commit/6460307feb7824114d84bfe88ae5f76e99eb7b23))
- **train**: `open` 서비스로 학습을 시작할 수 있도록 하는 기능: 커밋이 너무 많아 compare 링크를 대신 기재합니다
([#5](https://github.com/mindslab-ai/minds-mrc/issues/5))([9930069](https://github.com/mindslab-ai/brain-mrc-train/compare/9930069...canary))
- **common**: `getProgress`, `getAllProgress`, `close`, `stop`, `removeBinary` 등의 서비스로 특정 uuid의 학습 상태를 확인, 전체 학습 상태 확인, 학습 리스트에서 삭제, 학습 중단, 생성된 파일 삭제 기능 추가 
([#101](https://github.com/mindslab-ai/brain-mrc-train/issues/101))([4e90033](https://github.com/mindslab-ai/brain-mrc-train/commit/4e90033a334bad0d9fb96e0066167a1d0b6b6b4a))
- **build**: 빌드 시 필요 리소스를 s3에서 다운로드하는 기능
([#88](https://github.com/mindslab-ai/brain-mrc-train/issues/88))([9b529a1](https://github.com/mindslab-ai/brain-mrc-train/commit/9b529a17c401df458244939c46f2a073a7b6439c))
- **run(prediction)**: 상위 n개의 정답을 출력하는 기능
([#56](https://github.com/mindslab-ai/brain-mrc-train/issues/56))([0a435e9](https://github.com/mindslab-ai/brain-mrc-train/commit/0a435e97823ad36024306b61e8e55ceb50c82358))
- **build**: python 인터프리터 충돌 등을 방지하기 위해 virtualenv에서 구동되도록 변경
([#107](https://github.com/mindslab-ai/brain-mrc-train/issues/107))([2a9a80b](https://github.com/mindslab-ai/brain-mrc-train/commit/2a9a80b6a610f0c02ae6bedbe85a129a18f79966))


### Enhancements

- **common**: 기존 강원대 코드에서 디렉토리 구조 전체적 변경
([#4](https://github.com/mindslab-ai/minds-mrc/issues/4))([5372cc2](https://github.com/mindslab-ai/minds-mrc/commit/5372cc29ecb028e565fd67a59cbe4014106d8b12))
- **run(common)**: grpc 서버, 클라이언트 테스트 스크립트 추가: `tests/server`, `tests/client`
([#18](https://github.com/mindslab-ai/minds-mrc/issues/18))([b588f44](https://github.com/mindslab-ai/minds-mrc/commit/b588f44984e315fac180af99ddf3a0cf68a1e3ff))
- **run(common)**: TLO 로그 남기는 테스트 스크립트 추가: `tests/client/mrc_client_test_tlo.py`
([#60](https://github.com/mindslab-ai/minds-mrc/issues/60))([6460307](https://github.com/mindslab-ai/minds-mrc/commit/6460307feb7824114d84bfe88ae5f76e99eb7b23))
- **application**: 엑소브레인 시멘틱 검색기를 붙여 자동으로 본문을 가져와 MRC까지 수행하는 테스트 스크립트 추가: `tests/exo_search`
([e4535ea](https://github.com/mindslab-ai/minds-mrc/commit/e4535ea640776bc53effbf88cd530a7f7234193d))
- **run(common)**: 실행 시 vocab를 pickle에서 읽어오지 않고 별도의 파일에서 로드하는 것으로 수정
([#1](https://github.com/mindslab-ai/minds-mrc/issues/1))([e71a21f](https://github.com/mindslab-ai/brain-mrc-train/commit/e71a21f5f8d2427b89ebc90a1e53b27fa5c8c0a9))
- **train**: 복수의 정답 학습에 반영: 학습 시 dev set에서 복수정답인 경우를 반영하여 update
([#50](https://github.com/mindslab-ai/minds-mrc/issues/50))([24dc147](https://github.com/mindslab-ai/brain-mrc-train/commit/24dc147db8066bc46d7f9b1583c087da39291e39))
- **common**: 형상관리 체계 정비 및 proto namespace 변경 등
([#51](https://github.com/mindslab-ai/brain-mrc-train/issues/51))([2ec1a1e](https://github.com/mindslab-ai/brain-mrc-train/commit/2ec1a1e7fa0c11ee5b183e8de424477ebf998205))
- **build**: 빌드 시 pb 파일이 자동으로 생성되도록 변경
([#88](https://github.com/mindslab-ai/brain-mrc-train/issues/88))([3fedf7f](https://github.com/mindslab-ai/brain-mrc-train/commit/3fedf7f6973e7790d0add30f8f8e3e6ecb37528c))
- **pre-pro(common)**: 전처리 과정에 answer index 검증 코드 추가
([#110](https://github.com/mindslab-ai/brain-mrc-train/issues/110))([f7ef708](https://github.com/mindslab-ai/brain-mrc-train/commit/f7ef70844208c3c8f387941790c6d947ea3c1b55))
- **pre-pro(en)**: 단어를 인덱싱하는 과정에서 에러가 발생할 가능성 제거
([#104](https://github.com/mindslab-ai/brain-mrc-train/issues/104))([d337b81](https://github.com/mindslab-ai/brain-mrc-train/pull/121/commits/d337b817a0016edc1c18c1e3127eae6f2f0ea832))
- **train**: Reader class의 중복 코드 제거
([#77](https://github.com/mindslab-ai/brain-mrc-train/issues/77))([e5bd66b](https://github.com/mindslab-ai/brain-mrc-train/pull/121/commits/e5bd66b1ba71003bf345036a410b67a05d2a77a3))


### Breaking Changes
None - Initial release 
